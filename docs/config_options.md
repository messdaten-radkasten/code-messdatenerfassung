# Config option guide

Hier werden alle Optionen für die aktuellste Version der Konfiguration beschrieben.
Die aktuelle Version der Konfiguration ist 1.0.0.

## main config object

erforderliche Felder:

* "version" → string: "1.0.0"
* "dbConfig" → DatabaseConfigObject: {}
* "sensorConfig" → array[SensorConfigObject]: []

optionale Felder:
* "log_values" → boolean: true
* "log_discarded" → boolean: false

## DatabaseConfigObject

optionale Felder:

* "append_date" → bool: true (ob das Datum an den Datenbanknamen angehängt werden soll)
* "base_name" → string: "data" (Der Basisname der Datenbank, wenn append_date true ist, wird das Datum angehängt)
* "directory" → string: "dataBases" (Das Verzeichnis, in dem die Datenbanken gespeichert werden)
* "worker_sleep" → float: 0.1 (Die Zeit in Sekunden, die der Worker schläft, bevor er wieder auf die Datenbank zugreift. Höhere Werte können zu besserer Performance führen, aber sorgen für längere Verzögerungen beim Schreiben in die Datenbank.)
* "worker_mutex_timeout" → float: 1 (Die Zeit in Sekunden, die der Worker auf das Mutex wartet, bevor er aufgibt und die Datenbank nicht schreibt. Dieser Wert sollte immer größer sein als der worker_sleep Wert.)
* "backup_interval_m" → float: 60 (Das Intervall in Minuten, in dem einer Sicherheitskopie angelegt werden soll. <= 0 bedeutet, dass keine Sicherung durchgeführt wird.)
* "backup_max_files" → 10 (Die maximale Anzahl an Sicherungskopien, die gespeichert werden sollen. Wenn mehr Sicherungskopien vorhanden sind, werden die ältesten gelöscht.)
* "backup_subdir" → "backup" (Das Unterverzeichnis, relativ zu "directory", in dem die Sicherungskopien gespeichert werden sollen.)

## SensorConfigObject

erforderliche Felder:

* "type" → string: "GPS", "ADXL357" (Der Typ des Sensors)

### (optionale) Extra options für alle Sensoren:

* "table_name" → string: "" (Der Name der Tabelle, in der die Daten gespeichert werden, der standard ist abhängig vom Typ des Sensors)

### (optionale) Extra options für GPS:

***Wichtig:***
Es sollte nur ein GPSD Prozess laufen, da es sonst zu Problemen kommen kann.
GPSD kann mehrere GPS Geräte gleichzeitig verwalten und es werden automatisch alle Daten von allen Geräten empfangen und aufgezeichnet.

* "timeout" → int: 60 (in Sekunden, die Zeit die gewartet wird bis gpsd eine Antwort sendet)
* "polling_rate" → float: 10.0 (in Hz, bei zu hohen Werten wird potenziell nur 1Hz ausgegeben)

### (optionale) Extra options für ADXL357:

* "connectionType" → string: "I2C" oder "SPI"
* "m_range" → int: 10, 20, 40 (in g der Messbereich des Sensors)
* "no_temp" → boolean: false (ob der Sensor die Temperatur messen soll, wenn true wird das Register im sensor entsprechend eingestellt)
* "bus_id" → int: 1 (Die I2C/SPI Bus ID, auf dem der Sensor angeschlossen ist)
* "measurement_packets" → int: 1 (Anzahl der Messungsbefehle, kann helfen bei thread Problemen)
* "poll_sleep" → float: 0.1 (Die Zeit in Sekunden, die der Worker schläft, bevor er wieder auf den Sensor zugreift. Höhere Werte können zu besserer Performance führen, aber sorgen für längere Verzögerungen beim Messen.)

#### (optionale) I2C optionen:

* "address" → string: "0x1D" (Die Adresse des Sensors, als hexadezimaler String)
* "high_speed" → boolean: false (I2C high speed mode, des Sensors 3,4MHz)

#### (optionale) SPI optionen:

* "address" → int: 1 (Die Adresse des Sensors, als int)
* "spi_speed" → int: 10000000 (Die Geschwindigkeit des SPI Busses in Hz)

