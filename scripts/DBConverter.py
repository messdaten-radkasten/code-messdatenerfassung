#!/usr/bin/env python3

import argparse
import os
import sqlite3
import textwrap
from typing import List, Dict
from tqdm import trange, tqdm

old_table_create_str = '''
    CREATE TABLE IF NOT EXISTS sensor_data (
        time REAL NOT NULL,
        ns VARCHAR(1) NOT NULL,
        lat REAL NOT NULL,
        ow VARCHAR(1) NOT NULL,
        lon REAL NOT NULL,
        alt REAL NOT NULL,
        utc REAL NOT NULL,
        x REAL,
        y REAL,
        z REAL
    )
'''

old_insert_str = '''
    INSERT INTO sensor_data (
        time,
        ns, lat,
        ow, lon,
        alt,
        utc,
        x, y, z
    ) VALUES (
        :time,
        :ns, :lat,
        :ow, :lon,
        :alt,
        :utc,
        :x, :y, :z
    )
'''

description = '''
Konvertiert eine Datenbank im neuen Format in das alte Format.
'''

epilog = '''
Beispiele:
- pipenv run python3 ./scripts/DBConverter.py -i dataBases/data2024-04-11.db -o dataBases/data2024-04-11_old.db
- pipenv run python3 ./scripts/DBConverter.py -i dataBases/data2024-04-11.db --accel_table_name ADXL357_I2C
'''

if __name__ == "__main__":
    main_script_dir = os.path.dirname(__file__)

    parser = argparse.ArgumentParser(
        prog='DBConverter', formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(description), epilog=textwrap.dedent(epilog)
    )

    parser.add_argument(
        '--input', '-i', dest='input_file', action='store', help='Wähle die zu konvertierende Datenbank Datei aus.'
    )

    parser.add_argument(
        '--output',
        '-o',
        dest='output_file',
        action='store',
        help='Wähle den Namen der konvertierten Datenbank Datei aus. Default: <input_file>_old.db'
    )

    parser.add_argument(
        '--gps_table_name',
        dest='gps_table_name',
        action='store',
        default='gps_data',
        help='Wähle den Namen der Tabelle für GPS Daten aus. Default: gps_data'
    )

    parser.add_argument(
        '--accel_table_name',
        dest='accel_table_name',
        action='store',
        default='adxl357_data',
        help='Wähle den Namen der Tabelle für Beschleunigungssensordaten aus. Default: adxl357_data'
    )

    cli_args = vars(parser.parse_args())

    infile = os.path.abspath(cli_args['input_file'])
    if not os.path.exists(infile):
        print('Die angegebene Eingabedatei existiert nicht')
        exit(1)

    if 'output_file' not in cli_args or cli_args['output_file'] is None:
        outfile = infile + '_old.db'
    else:
        outfile = cli_args['output_file']

    outfile = os.path.abspath(outfile)
    if os.path.exists(outfile):
        print('Die angegebene Ausgabedatei existiert bereits')
        exit(1)

    # Open the input and output databases
    orig_db = sqlite3.connect(infile)
    orig_cursor = orig_db.cursor()

    new_db = sqlite3.connect(outfile)
    new_cursor = new_db.cursor()

    # create the table in the new database
    new_cursor.execute(old_table_create_str)

    # Get a list of all the table names in the input database
    table_names = orig_cursor.execute("SELECT name FROM sqlite_master WHERE type='table';").fetchall()
    table_names = [t[0] for t in table_names]
    print('Old Tables:', table_names)

    if cli_args['gps_table_name'] not in table_names:
        print('The specified GPS table does not exist in the input database')
        exit(1)

    if cli_args['accel_table_name'] not in table_names:
        print('The specified Accelerometer table does not exist in the input database')
        exit(1)

    # Get the GPS data from the input database
    gps_data = orig_cursor.execute(f"SELECT timestamp, gps_time, latitude, longitude, altitude "
                                   f"FROM {cli_args['gps_table_name']};").fetchall()
    gps_point_count = len(gps_data)
    print('Found', gps_point_count, 'GPS data points')

    print(gps_data[0])

    for i_gps in trange(gps_point_count - 1):
        current_point = gps_data[i_gps]
        next_point = gps_data[i_gps + 1]

        to_be_added_point: Dict = {
            'utc': current_point[1],
            'lat': current_point[2],
            'lon': current_point[3],
            'alt': current_point[4],
            'ns': 'N',
            'ow': 'O'
        }

        if to_be_added_point['lat'] < 0:
            to_be_added_point['lat'] = -to_be_added_point['lat']
            north_south = 'S'

        if to_be_added_point['lon'] < 0:
            to_be_added_point['lon'] = -to_be_added_point['lon']
            east_west = 'W'

        # get the accelerometer data between the current and next GPS points
        accel_data = orig_cursor.execute(
            f"SELECT timestamp,accel_x,accel_y,accel_z FROM {cli_args['accel_table_name']} "
            f"WHERE timestamp BETWEEN {current_point[0]} AND {next_point[0]};"
        ).fetchall()
        accel_points_count = len(accel_data)

        for i_accel in range(accel_points_count):
            accel_point = accel_data[i_accel]

            to_be_added_point['time'] = accel_point[0]
            to_be_added_point['x'] = accel_point[1]
            to_be_added_point['y'] = accel_point[2]
            to_be_added_point['z'] = accel_point[3]

            # add the point to the new database
            new_cursor.execute(old_insert_str, to_be_added_point)

        new_db.commit()
