import argparse
import os
import subprocess
import textwrap

description = '''
Ein Script, um mit dem Radkasten-Service zu interagieren.
Das Script muss als root ausgeführt werden, um den Service zu steuern.
Wenn das nicht der Fall ist, wird das Script mit einem Fehler beendet.
'''

epilog = '''
Befehle:
- start: Startet den Radkasten-Service
- stop: Stoppt den Radkasten-Service
- restart: Startet den Radkasten-Service neu
- reload: Lädt den Radkasten-Service neu

- enable: Aktiviert den Radkasten-Service beim Systemstart
- disable: Deaktiviert den Radkasten-Service beim Systemstart

- status: Zeigt den Status des Radkasten-Service an
- journal: Zeigt das Journal des Radkasten-Service an (Detaillierter Log)

- pipenv_setup: Installiert die Python-Abhängigkeiten für den Radkasten-Service
- update: Aktualisiert den Radkasten-Service (muss ggf. zwei mal ausgeführt werden)

Beispiele:
- Starte den Radkasten-Service: sudo python3 {dir}/ServiceManager.py start
- Stoppe den Radkasten-Service: sudo python3 {dir}/ServiceManager.py stop
- Zeige die Logs des Radkasten-Service an: sudo python3 {dir}/ServiceManager.py journal
- Installiere die Python-Abhängigkeiten: sudo python3 {dir}/ServiceManager.py pipenv_setup
'''

if __name__ == "__main__":
    main_script_dir = os.path.abspath(os.path.dirname(__file__))

    parser = argparse.ArgumentParser(
        prog='ServiceManager', formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(description), epilog=textwrap.dedent(epilog.format(dir=main_script_dir))
    )

    parser.add_argument(
        'command',
        action='store',
        choices=[
            'start', 'stop', 'restart', 'status', 'enable', 'disable', 'reload', 'journal', 'pipenv_setup', 'update'
        ],
        help='Der Befehl mit dem der Service gesteuert werden soll.'
    )

    cli_args = vars(parser.parse_args())

    # prüfe ob das Script als root ausgeführt wird
    if os.geteuid() != 0:
        print('Fehler: sudo-Zugriff benötigt')
        exit(1)

    match cli_args['command']:
        case 'start':
            subprocess.run(['systemctl', 'start', 'radkasten.service'])
        case 'stop':
            subprocess.run(['systemctl', 'stop', 'radkasten.service'])
        case 'restart':
            subprocess.run(['systemctl', 'restart', 'radkasten.service'])
        case 'status':
            subprocess.run(['systemctl', 'status', 'radkasten.service'])
        case 'enable':
            subprocess.run(['systemctl', 'enable', 'radkasten.service'])
        case 'disable':
            subprocess.run(['systemctl', 'disable', 'radkasten.service'])
        case 'reload':
            subprocess.run(['systemctl', 'reload', 'radkasten.service'])
        case 'journal':
            subprocess.run(['journalctl', '-xeu', 'radkasten.service'])
        case 'pipenv_setup':
            subprocess.run(['pipenv', 'install', '--python', 'python3.12'], cwd='/opt/radkasten')
            subprocess.run(['pipenv', 'update'], cwd='/opt/radkasten')
        case 'update':
            subprocess.run(['git', 'pull'], cwd='/opt/radkasten')
            subprocess.run(['pipenv', 'install', '--python', 'python3.12'], cwd='/opt/radkasten')
            subprocess.run(['python3', 'scripts/Setup.py', '--no_git'], cwd='/opt/radkasten')
        case _:
            print('Unbekannter Befehl')
            exit(1)
