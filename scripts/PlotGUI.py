import asyncio
import multiprocessing
import subprocess
import sys
import sqlite3
import os
from typing import List, Dict

import gi
import cairo

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal

gi.require_version("Gtk", "4.0")
from gi.repository import Gtk, Gio, Gdk

main_script_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))

cmaps = [('Perceptually Uniform Sequential', [
            'viridis', 'plasma', 'inferno', 'magma', 'cividis']),
         ('Sequential', [
            'Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
            'YlOrBr', 'YlOrRd', 'OrRd', 'PuRd', 'RdPu', 'BuPu',
            'GnBu', 'PuBu', 'YlGnBu', 'PuBuGn', 'BuGn', 'YlGn']),
         ('Sequential (2)', [
            'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink',
            'spring', 'summer', 'autumn', 'winter', 'cool', 'Wistia',
            'hot', 'afmhot', 'gist_heat', 'copper']),
         ('Diverging', [
            'PiYG', 'PRGn', 'BrBG', 'PuOr', 'RdGy', 'RdBu',
            'RdYlBu', 'RdYlGn', 'Spectral', 'coolwarm', 'bwr', 'seismic']),
         ('Cyclic', ['twilight', 'twilight_shifted', 'hsv']),
         ('Qualitative', [
            'Pastel1', 'Pastel2', 'Paired', 'Accent',
            'Dark2', 'Set1', 'Set2', 'Set3',
            'tab10', 'tab20', 'tab20b', 'tab20c']),
         ('Miscellaneous', [
            'flag', 'prism', 'ocean', 'gist_earth', 'terrain', 'gist_stern',
            'gnuplot', 'gnuplot2', 'CMRmap', 'cubehelix', 'brg',
            'gist_rainbow', 'rainbow', 'jet', 'turbo', 'nipy_spectral',
            'gist_ncar'])]


class MyApplication(Gtk.Application):
    database_file = ''
    db: sqlite3.Connection
    cursor: sqlite3.Cursor

    main_window: Gtk.ApplicationWindow
    error_window: Gtk.Window

    table_list: Gtk.ListBox
    row_list: Gtk.ListBox

    cmap_btn_list: Gtk.ListBox
    cmap_draw_areas: List[Gtk.DrawingArea] = []
    cmap_gradients: Dict[str, cairo.Gradient] = {}
    cmap_gradient_steps: int = 128

    fft_mode_dropdown: Gtk.DropDown
    fft_scale_dropdown: Gtk.DropDown

    print_ts: bool = False
    print_data: bool = False
    data_raw_ts: bool = False
    print_fft: bool = True
    fft_slider_value: float = 1.0
    gauss_slider_value: float = 12.0

    selected_cmap: str = 'nipy_spectral'
    reverse_cmap: bool = False
    selected_table: str = ''
    selected_rows: List[str] = []

    def __init__(self):
        super().__init__(application_id='de.thm.radkasten.plotgui')

    def do_activate(self):
        builder = Gtk.Builder()
        ui_file = os.path.join(main_script_dir, 'plotgui.ui')
        builder.add_from_file(ui_file)

        # Obtain the button widget and connect it to a function
        # button = builder.get_object("button1")
        # button.connect("clicked", self.hello)

        # Obtain and show the main window
        self.main_window = builder.get_object("PlotAppWindow")
        self.error_window = builder.get_object("ErrorDialog")

        builder.get_object("Error_dismiss_btn").connect("clicked", lambda _: self.error_window.set_visible(False))

        # Obtain the table and row list
        self.table_list = builder.get_object("DB_table_list")
        self.row_list = builder.get_object("DB_row_list")

        # setup the colormap buttons
        self.cmap_btn_list = builder.get_object("cmap_btn_list")
        self.__setup_cmap_buttons()

        # setup the function to create the graph
        builder.get_object("create_graph_btn").connect("clicked", self.create_graphs)

        # setup the graph config options
        builder.get_object("plot_ts_btn").connect("toggled", self.__on_ts_toggle)
        builder.get_object("plot_data_btn").connect("toggled", self.__on_data_toggle)
        builder.get_object("plot_raw_data_ts_btn").connect("toggled", self.__on_raw_ts_toggle)
        builder.get_object("plot_fft_btn").connect("toggled", self.__on_fft_toggle)

        builder.get_object("fft_size_slider").connect("value-changed", self.__on_fft_slider_changed)
        builder.get_object("gauss_size_slider").connect("value-changed", self.__on_gauss_slider_changed)

        self.fft_mode_dropdown = builder.get_object("fft_mode_dropdown")
        self.fft_scale_dropdown = builder.get_object("fft_scale_dropdown")

        # Setup the menu
        self.__create_menu_actions()
        self.set_menubar(builder.get_object("MainMenu"))

        self.main_window.set_application(self)
        self.main_window.present()

    def do_startup(self):
        Gtk.Application.do_startup(self)

    def __create_menu_actions(self):
        act_opendb = Gio.SimpleAction.new('opendb', None)
        Gio.ActionMap.add_action(self, act_opendb)
        act_opendb.connect('activate', self.__select_db, self)

        act_quit = Gio.SimpleAction.new('quit', None)
        Gio.ActionMap.add_action(self, act_quit)
        act_quit.connect('activate', self.__on_close, self)

    def __on_close(self, *_):
        self.main_window.close()

    def __select_db(self, *_):
        dialog = Gtk.FileDialog(title="Datenbank auswählen")

        _filter = Gtk.FileFilter()
        _filter.set_name("SQLite Datenbanken")
        _filter.add_mime_type("application/x-sqlite3")
        dialog.set_default_filter(_filter)

        dialog.open(parent=self.main_window, callback=self._db_file_selected)

    def _db_file_selected(self, source_dialog, res):
        try:
            file = source_dialog.open_finish(res)

            self.database_file = file.get_path()
            self.db = sqlite3.connect(self.database_file)
            self.cursor = self.db.cursor()
            print(f"Selected database: {self.database_file}")

            # Get the tables
            tables = self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall()
            print(tables)

            self.table_list.remove_all()
            self.row_list.remove_all()
            self.selected_rows = []

            first_btn = None
            for table in tables:
                table_btn = Gtk.ToggleButton.new_with_label(table[0])
                table_btn.connect("clicked", self.__on_table_selected, table[0])

                if first_btn is None:
                    first_btn = table_btn
                else:
                    table_btn.set_group(first_btn)

                self.table_list.append(table_btn)

        except Exception as e:
            pass

    def __on_table_selected(self, btn, table_name):
        print(f"Selected table: {table_name}")

        self.row_list.remove_all()
        self.selected_table = table_name
        self.selected_rows = []

        # get all columns of the table
        columns = self.cursor.execute(f"PRAGMA table_info({table_name})").fetchall()
        print(columns)

        for col in columns:
            col_btn = Gtk.ToggleButton.new_with_label(col[1])
            col_btn.connect("clicked", self.__on_column_selected, col[1])
            self.row_list.append(col_btn)

    def __on_column_selected(self, btn, column_name):
        if btn.get_active():
            self.selected_rows.append(column_name)
            print(f"Selected column: {column_name}")
        else:
            self.selected_rows.remove(column_name)
            print(f"Deselected column: {column_name}")

    def __on_ts_toggle(self, btn):
        self.print_ts = btn.get_active()

    def __on_fft_toggle(self, btn):
        self.print_fft = btn.get_active()

    def __on_data_toggle(self, btn):
        self.print_data = btn.get_active()

    def __on_raw_ts_toggle(self, btn):
        self.data_raw_ts = btn.get_active()

    def __on_fft_slider_changed(self, slider):
        self.fft_slider_value = slider.get_value()
        print(f"FFT Slider: {self.fft_slider_value}")

    def __on_gauss_slider_changed(self, slider):
        self.gauss_slider_value = slider.get_value()
        print(f"Gauss Slider: {self.gauss_slider_value}")

    def __on_cmap_r_toggle(self, btn):
        self.reverse_cmap = btn.get_active()

    def __on_cmap_samples_changed(self, slider):
        new_steps = int(slider.get_value())
        if new_steps == self.cmap_gradient_steps:
            return

        self.cmap_gradient_steps = new_steps
        self.cmap_gradients.clear()
        self.cmap_gradients = {}
        print(f"Changed cmap samples to {self.cmap_gradient_steps}")

        for area in self.cmap_draw_areas:
            area.queue_draw()

    def __setup_cmap_buttons(self):
        reverse_btn = Gtk.ToggleButton.new_with_label("Reverse Colormap")
        reverse_btn.connect("toggled", self.__on_cmap_r_toggle)
        self.cmap_btn_list.append(reverse_btn)

        samples_adj = Gtk.Adjustment.new(128, 16, 1024, 8, 64, 0)
        samples_scale = Gtk.Scale.new(Gtk.Orientation.HORIZONTAL, samples_adj)
        samples_scale.set_digits(0)
        samples_scale.set_value_pos(Gtk.PositionType.RIGHT)
        samples_scale.set_hexpand(True)
        samples_scale.add_mark(16, Gtk.PositionType.TOP, "16")
        samples_scale.add_mark(64, Gtk.PositionType.TOP, "64")
        samples_scale.add_mark(128, Gtk.PositionType.TOP, "128")
        samples_scale.add_mark(256, Gtk.PositionType.TOP, "256")
        samples_scale.add_mark(512, Gtk.PositionType.TOP, "512")
        samples_scale.add_mark(1024, Gtk.PositionType.TOP, "1024")
        samples_scale.connect("value-changed", self.__on_cmap_samples_changed)
        self.cmap_btn_list.append(samples_scale)

        first_btn = None
        for map_type in cmaps:
            header = Gtk.Label.new(map_type[0])
            header.set_halign(Gtk.Align.START)
            self.cmap_btn_list.append(header)

            for cmap in map_type[1]:
                radio = Gtk.ToggleButton.new_with_label(cmap)
                radio.connect("toggled", self.__on_cmap_selected, cmap)
                if first_btn is None:
                    first_btn = radio
                else:
                    radio.set_group(first_btn)

                if cmap == self.selected_cmap:
                    radio.set_active(True)

                cmap_draw_area = Gtk.DrawingArea()
                cmap_draw_area.set_size_request(512, 20)
                cmap_draw_area.set_draw_func(self.__draw_cmap, cmap)
                self.cmap_draw_areas.append(cmap_draw_area)

                cmap_pane = Gtk.Paned.new(Gtk.Orientation.HORIZONTAL)
                cmap_pane.set_resize_end_child(False)
                cmap_pane.set_end_child(cmap_draw_area)
                cmap_pane.set_start_child(radio)
                self.cmap_btn_list.append(cmap_pane)

    def __get_or_create_cmap_gradient(self, cmap_name) -> cairo.Gradient:
        if cmap_name in self.cmap_gradients:
            return self.cmap_gradients[cmap_name]

        cmap = plt.get_cmap(cmap_name)
        gradient = np.linspace(0.0, 1.0, self.cmap_gradient_steps)

        c_gradient = cairo.LinearGradient(0, 0, 1.0, 0)
        for x in gradient:
            r, g, b, _ = cmap(x)
            c_gradient.add_color_stop_rgb(x, r, g, b)

        self.cmap_gradients[cmap_name] = c_gradient
        return c_gradient

    def __draw_cmap(self, drawing_area, cr, width, height, cmap_name):
        gradient = self.__get_or_create_cmap_gradient(cmap_name)
        gradient.set_filter(cairo.Filter.BILINEAR)
        gradient.set_matrix(cairo.Matrix(xx=1.0/float(width)))

        cr.rectangle(0, 0, width, height)
        cr.set_source(gradient)
        cr.fill()

    def __on_cmap_selected(self, btn, cmap_name):
        if btn.get_active():
            self.selected_cmap = cmap_name
            print(f"Selected colormap: {cmap_name}")

    def create_graphs(self, btn):
        timestamp_only = not self.print_data and not self.print_fft

        if timestamp_only and not self.print_ts:
            self.error_window.present()
            return

        # to plot the data we need at least a table and a column
        # in case we only want to plot the timestamps we need only need a table
        if timestamp_only:
            if not self.selected_table:
                self.error_window.present()
                return
        else:
            if not self.selected_rows:
                self.error_window.present()
                return

        args = ['-i', self.database_file, '-t', self.selected_table]

        if not timestamp_only:
            for row in self.selected_rows:
                args.extend(['-c', row])

            if not self.print_ts:
                args.append('--no_ts')

        if self.print_data:
            if self.data_raw_ts:
                args.append('--raw_timestamps')
        else:
            args.append('--no_data_plot')

        if self.print_fft:
            args.extend(['--fft_size', str(self.fft_slider_value)])
            args.extend(['--gauss_size', str(self.gauss_slider_value)])

            args.extend(['--fft_mode', self.fft_mode_dropdown.get_selected_item().get_string()])
            args.extend(['--fft_scale', self.fft_scale_dropdown.get_selected_item().get_string()])

            cmap = self.selected_cmap
            if self.reverse_cmap:
                cmap += '_r'

            args.extend(['--cmap', cmap])
        else:
            args.append('--no_fft_plot')

        print(args)

        plot_script = os.path.join(main_script_dir, 'SimplePlot.py')
        args = [plot_script] + args

        p = multiprocessing.Process(target=self.plot_data, args=(args,))
        p.start()

    @staticmethod
    def plot_data(args):
        proc = subprocess.run(['python3'] + args)
        print(f"Plot Process finished with exit code {proc.returncode}")


if __name__ == '__main__':
    application = MyApplication()
    exit_status = application.run(sys.argv)
    sys.exit(exit_status)
