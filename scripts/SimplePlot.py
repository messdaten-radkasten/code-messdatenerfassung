import argparse
import os
import sqlite3
import textwrap
from typing import List, Dict

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal


def make_data_const(timestamps, data):
    """
    Korriegiert die Daten, sodass die Zeidifferenzen konstant sind.
    Die Daten werden linear interpoliert, um die Daten für eine fft und ähnliche Analysen brauchbar zu machen.
    :param timestamps: Die Zeitstempel der Daten
    :param data: Die Daten
    :return: Die korrigierten Zeitstempel und Daten
    """

    sample_count = len(timestamps)
    delta_t = (timestamps[-1] - timestamps[0]) / sample_count
    if sample_count != len(data):
        raise ValueError('Die Anzahl der Zeitstempel und Daten stimmt nicht überein')

    new_timestamps = np.arange(timestamps[0], timestamps[-1], delta_t)
    new_data = np.interp(new_timestamps, timestamps, data)

    return new_timestamps, new_data


description = '''
Ein einfaches Script zum Plotten von Daten.
Es muss ein Eingabedatei, eine Tabelle angegeben werden.
Eine Analyse der Timestamp-Differenzen wird immer durchgeführt und geplottet.
Beispiel nur timestamp:
    pipenv run python3 ./scripts/SimplePlot.py -i dataBases/data2024-04-11.db -t ADXL357_I2C

Wenn mindestens eine Spalte angegeben wird, werden die Daten geplottet.
Alle Spalten werden in der gleichen Grafik geplottet mit den Timestamps auf der x-Achse.
Wichtig: Der Graph wird erst angezeigt, nachdem der Timestamp-Graph geschlossen wurde.
Beispiel mit Spalten:
    pipenv run python3 ./scripts/SimplePlot.py -i dataBases/data2024-04-11.db -t ADXL357_I2C -c accel_x -c accel_y

Die erlaubten Colormaps sind unter folgendem Link zu finden: https://matplotlib.org/stable/users/explain/colors/colormaps.html
'''

if __name__ == "__main__":
    main_script_dir = os.path.dirname(__file__)

    parser = argparse.ArgumentParser(
        prog='SimplePlot', formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(description)
    )

    parser.add_argument(
        '--input', '-i', dest='input_file', action='store', help='Wähle die zu plottende Sqlite Datenbank Datei aus.'
    )

    parser.add_argument(
        '--table', '-t', dest='table_name', action='store', help='Wähle die zu plottende Tabelle aus.'
    )

    # the argument for the columns to plot, this argument can be repeated
    parser.add_argument(
        '--column', '-c', dest='columns', action='append',
        help='Wähle die zu plottenden Spalten aus. Kann mehrfach verwendet werden.'
    )

    parser.add_argument('--cmap', dest='cmap', action='store', default='PuBuGn',
                        help='Wähle die Farbkarte für die FFT aus.')

    parser.add_argument('--no_ts', dest='draw_timestamps', action='store_false', default=True,
                        help='Zeichne die timestamp-Differenzen nicht.')

    parser.add_argument('--no_data_plot', dest='draw_data', action='store_false', default=True,
                        help='Zeichne die Rohdaten nicht.')

    parser.add_argument('--no_fft_plot', dest='draw_fft', action='store_false', default=True,
                        help='Zeichne die FFT nicht.')

    parser.add_argument('--raw_timestamps', dest='raw_timestamps', action='store_true', default=False,
                        help='Zeige die Absoluten Timestamps an, statt der Zeit seit Messbeginn.')

    parser.add_argument('--fft_size', dest='fft_size', action='store', default=1.0,
                        help='Sprunggröße für die FFT in Sekunden.')

    parser.add_argument('--fft_mode', dest='fft_mode', action='store', default='onesided2X',
                        choices=['onesided', 'onesided2X', 'twosided', 'centered'],
                        help='Der Modus für die FFT Berechnung.')

    parser.add_argument('--fft_scale', dest='fft_scale', action='store', default='magnitude',
                        choices=['magnitude', 'psd'],
                        help='Der Modus für die FFT Skalierung.')

    parser.add_argument('--gauss_size', dest='gauss_size', action='store', default=12.0,
                        help='Die Größe des Gauß-Fensters als Anteil der Samplerate.')

    cli_args = vars(parser.parse_args())

    if not cli_args['draw_timestamps'] and not cli_args['draw_data'] and not cli_args['draw_fft']:
        print('Fehler: Es muss mindestens ein Plot-Typ ausgewählt werden')
        exit(1)

    if not cli_args['input_file']:
        print('Fehler: Keine Eingabedatei angegeben')
        exit(1)

    if not os.path.exists(cli_args['input_file']):
        print('Fehler: Die angegebene Eingabedatei existiert nicht')
        exit(1)

    if not cli_args['table_name']:
        print('Fehler: Keine Tabelle angegeben')
        exit(1)

    timestamp_only = False
    if not cli_args['columns']:
        print('Nur die Timestamp-Spalte kann geplottet werden')
        timestamp_only = True

    infile = os.path.abspath(cli_args['input_file'])
    table_name = cli_args['table_name']
    columns = cli_args['columns']

    db = sqlite3.connect(infile)
    cursor = db.cursor()

    # check if the table exists
    cursor.execute(f"SELECT name FROM sqlite_master WHERE type='table' AND name='{table_name}'")
    if not cursor.fetchone():
        print(f'Die Tabelle {table_name} existiert nicht')
        exit(1)

    # check if the columns exist
    cursor.execute(f"PRAGMA table_info({table_name})")
    table_info = cursor.fetchall()
    table_columns = [col[1] for col in table_info]

    if 'timestamp' not in table_columns:
        print('Die Timestamp-Spalte konnte nicht in der Tabelle gefunden werden')
        exit(1)

    ts_data = cursor.execute(f"SELECT timestamp FROM {table_name}").fetchall()
    ts_data = np.array(ts_data).flatten()
    ts_rel_data = ts_data - ts_data[0]
    ts_diffs = np.diff(ts_data)

    if cli_args['draw_timestamps']:
        # plot the timestamp diffs and a histogram with a log scale
        fig, plots = plt.subplots(2, 1)
        plots[0].plot(ts_diffs)
        plots[0].set_title('Timestamp Differenzen')
        plots[0].set_xlabel('Index')
        plots[0].set_ylabel('Differenz in Sekunden')

        plots[1].hist(ts_diffs, bins=50)
        plots[1].set_title('Histogramm der Timestamp Differenzen')
        plots[1].set_xlabel('Differenz in Sekunden')
        plots[1].set_ylabel('Anzahl der Vorkommen')
        plots[1].set_yscale('log')

        plt.show()

    if timestamp_only:
        exit(0)

    if not cli_args['draw_data'] and not cli_args['draw_fft']:
        exit(0)

    # check if all columns exist
    for col in columns:
        if col not in table_columns:
            print(f'Die Spalte {col} existiert nicht')
            exit(1)

    # get the data from the database
    data: Dict[str, List[float]] = {col: [] for col in columns}
    for col in columns:
        data[col] = cursor.execute(f"SELECT {col} FROM {table_name}").fetchall()
        data[col] = np.array(data[col]).flatten()

    # plot the data
    subplots = 0
    if cli_args['draw_data']:
        subplots += 1
    if cli_args['draw_fft']:
        subplots += len(columns)

    fig, ax = plt.subplots(subplots, 1)

    if cli_args['draw_data']:
        if subplots == 1:
            data_ax = ax
        else:
            data_ax = ax[0]

        for col in columns:
            if cli_args['raw_timestamps']:
                data_ax.plot(ts_data, data[col], label=col)
            else:
                data_ax.plot(ts_rel_data, data[col], label=col)

        data_ax.set_title('Daten')
        data_ax.set_xlabel('Timestamp')
        data_ax.set_ylabel('Wert')
        data_ax.legend()

    if cli_args['draw_fft']:

        corrected_data = {}
        for col in columns:
            corrected_data[col] = make_data_const(ts_data, data[col])

        delta_t = corrected_data[columns[0]][0][1] - corrected_data[columns[0]][0][0]
        samplerate = 1 / delta_t
        samples_per_second = int(samplerate)
        print(f'Samplerate: {samplerate} Hz\nSamples per second: {samples_per_second}\nDelta t: {delta_t} s\n')

        fft_size = float(cli_args['fft_size'])
        mfft = int(fft_size * samples_per_second)

        # standard deviation for Gaussian window in samples
        gauss_size_divider = float(cli_args['gauss_size'])
        g_std = int(samples_per_second / gauss_size_divider)
        gauss_size = int(3 * samples_per_second / gauss_size_divider)
        hop = int(gauss_size / 4)
        print(f'Gaussian window size: {gauss_size}\nStandard deviation: {g_std}\nHop: {hop}\nMFFT: {mfft}\n')

        w = scipy.signal.windows.gaussian(gauss_size, std=g_std, sym=True)

        axis_offset = 0
        if cli_args['draw_data']:
            axis_offset = 1

        if subplots == 1:
            ax = [ax]

        # make the data const and calculate the fft for all data series and plot it
        for i in range(len(columns)):
            col = columns[i]
            axis = ax[i + axis_offset]

            fft_fun = scipy.signal.ShortTimeFFT(w, hop=hop, mfft=mfft, fs=delta_t, scale_to=cli_args['fft_scale'],
                                                fft_mode=cli_args['fft_mode'])
            fft_data = fft_fun.spectrogram(corrected_data[col][1])
            im1 = axis.imshow(fft_data, origin='lower', aspect='auto', cmap=cli_args['cmap'])

            axis.set_title(f'FFT von {col}')
            axis.set_xlabel('Zeit in Sekunden')
            axis.set_ylabel('Frequenz in Hz')

    plt.show()
