import argparse
import os
import platform
import shutil
import subprocess

service_script = '''
# systemd unit datei für die Messdatenerfassung

[Unit]
Description=Radkasten Messdatenerfassung

Wants=chronyd.service
Wants=gpsd.service

After=chronyd.service
After=gpsd.service

[Service]
Type=simple
WorkingDirectory={main_script_dir}
ExecStartPre=-{pipenv_cmd} install --python /bin/python3.12
ExecStartPre=-{pipenv_cmd} update
ExecStart={pipenv_cmd} run main --config /etc/radkasten/config.json

# Disable Python's buffering of STDOUT and STDERR, so that output from the
# service shows up immediately in systemd's logs
Environment=PYTHONUNBUFFERED=1

# Automatically restart the service if it crashes
Restart=always

[Install]
# Tell systemd to automatically start this service when the system boots
# (assuming the service is enabled)
WantedBy=default.target
'''

chrony_gpsd_file = '''
# set larger delay to allow the NMEA source to overlap with
# the other sources and avoid the falseticker status
refclock SHM 0 refid GPS precision 1e-1 offset 0.9999 delay 0.2
refclock SHM 1 refid PPS precision 1e-7
'''

package_list = [
    'python3.12-full', 'python3.12-dev', 'python3-pip', 'python3-wheel', 'python3-setuptools', 'pipenv',
    'gpsd', 'gpsd-tools', 'git', 'chrony', 'fake-hwclock', 'doxygen', 'wget', 'unzip', 
    'libgirepository-1.0-dev', 'gobject-introspection', 'libgtkmm-4.0-dev'
]


def install_dependencies_apt(dummy: bool):
    if dummy:
        print('Führe apt-get update aus...')
        print('Führe apt-get install {} aus...'.format(' '.join(package_list)))
    else:
        subprocess.run(['sudo', 'apt-get', 'update'])
        subprocess.run(['sudo', 'apt-get', 'install', '-y'] + package_list)


def install_dependencies_pacman(dummy: bool):
    if dummy:
        print('Führe pacman -Syu aus...')
        print('Führe pacman -S {} aus...'.format(' '.join(package_list)))
    else:
        subprocess.run(['sudo', 'pacman', '-Syu'])
        subprocess.run(['sudo', 'pacman', '-S'] + package_list)


def install_dependencies(dummy: bool, force: bool):
    if shutil.which('apt-get'):
        install_dependencies_apt(dummy)
    elif shutil.which('pacman'):
        install_dependencies_pacman(dummy)
    else:
        print('Kein Paketmanager gefunden. Bitte installiere die folgenden Pakete manuell:')
        print('gpsd pipenv git chrony fake-hwclock doxygen dot wget unzip')

    if dummy:
        print('Schreibe chrony GPSD Konfigurationsdatei /etc/chrony/conf.d/gpsd.conf:')
        print(chrony_gpsd_file)
    else:
        if os.path.exists('/etc/chrony/conf.d/gpsd.conf') and not force:
            print('Die Chrony Konfigurationsdatei existiert bereits. Verwende --force, um sie zu überschreiben.')
            return

        echo_string = subprocess.Popen(('echo', chrony_gpsd_file), stdout=subprocess.PIPE)
        subprocess.check_output(('sudo', 'tee', '/etc/chrony/conf.d/gpsd.conf'), stdin=echo_string.stdout)
        echo_string.wait()


def get_or_update_repo(dummy: bool, install_location: str):
    if os.path.exists(install_location):
        if dummy:
            print('Führe git pull aus...')
        else:
            subprocess.run(['git', 'pull'], cwd=install_location)
    else:
        if dummy:
            print('Führe git clone aus...')
        else:
            subprocess.run([
                'git', 'clone', 'https://git.thm.de/messdaten-radkasten/code-messdatenerfassung', install_location
            ])


def install_systemd_service(dummy: bool, install_location: str):
    fill_dict = {
        'pipenv_cmd': shutil.which('pipenv'),
        'main_script_dir': install_location,
    }

    if dummy:
        print('Schreibe Systemd Service Datei...')
        print(service_script.format(**fill_dict))
    else:
        try:
            subprocess.run(['systemctl', 'stop', 'radkasten.service'])
        except subprocess.CalledProcessError:
            pass

        with open('/etc/systemd/system/radkasten.service', 'w') as f:
            f.write(service_script.format(**fill_dict))

        subprocess.run(['systemctl', 'daemon-reload'])
        subprocess.run(['systemctl', 'enable', 'radkasten.service'])


def install_config(install_location: str, force: bool = False, dummy: bool = False, local_install: bool = False):
    if local_install:
        new_config = os.path.abspath(os.path.join(install_location, 'config.json'))
    else:
        new_config = '/etc/radkasten/config.json'

    if os.path.exists(new_config) and not force:
        print('Die Konfigurationsdatei existiert bereits. Verwende --force, um sie zu überschreiben.')
        return

    if not dummy:
        if local_install:
            original_config = os.path.join(install_location, 'example_config.json')
            shutil.copy(os.path.abspath(original_config), new_config)
        else:
            os.makedirs('/etc/radkasten', exist_ok=True)
            original_config = os.path.join(install_location, 'example_config.json')
            shutil.copy(os.path.abspath(original_config), new_config)


if __name__ == "__main__":
    main_script_dir = os.path.dirname(__file__)

    parser = argparse.ArgumentParser(
        prog='Setup', description='Installiert dependencies und fügt den Systemd Service hinzu.'
    )

    parser.add_argument(
        '--force',
        dest='force',
        action='store_true',
        default=False,
        help='Schreibt die Systemd Service Datei auch, wenn sie bereits existiert. Default: False'
    )

    parser.add_argument(
        '--no-deps',
        dest='install_deps',
        action='store_false',
        default=True,
        help='Installiert keine dependencies. Default: False'
    )

    parser.add_argument(
        '--no_git',
        dest='update_git',
        action='store_false',
        default=True,
        help='Führt kein git pull aus.'
    )

    parser.add_argument(
        '--local_install',
        dest='local_install',
        action='store_true',
        default=False,
        help='Installiert die Abhängigkeiten lokal. Default: False'
    )

    parser.add_argument(
        '--install_location',
        dest='install_location',
        action='store',
        default='/opt/radkasten',
        help='Wähle den Installationsort aus. Default: /opt/radkasten'
    )

    parser.add_argument(
        '--dummy',
        dest='dummy',
        action='store_true',
        default=False,
        help='Führt das Skript im Dummy-Modus aus, es werden keine Änderungen vorgenommen. Default: False'
    )

    cli_args = vars(parser.parse_args())

    if platform.system() != 'Linux':
        print('Dieses Skript ist nur für Linux-Systeme geeignet.')
        exit(1)

    if cli_args['local_install']:
        if cli_args['install_location'] == '/opt/radkasten':
            cli_args['install_location'] = os.path.join(main_script_dir, '..')
    else:
        # prüfe ob das Script als root ausgeführt wird
        if os.geteuid() != 0:
            print('Fehler: sudo-Zugriff benötigt')
            exit(1)

    if cli_args['install_deps'] or cli_args['dummy']:
        install_dependencies(dummy=cli_args['dummy'], force=cli_args['force'])

    if cli_args['update_git'] or cli_args['dummy']:
        get_or_update_repo(dummy=cli_args['dummy'], install_location=cli_args['install_location'])

    install_config(
        dummy=cli_args['dummy'],
        force=cli_args['force'],
        install_location=cli_args['install_location'],
        local_install=cli_args['local_install']
    )

    if not cli_args['local_install']:
        install_systemd_service(dummy=cli_args['dummy'], install_location=cli_args['install_location'])

    print('Setup abgeschlossen.')
    print(f"Führe sicherheitshalber `sudo python3 {cli_args['install_location']}/scripts/Setup.py` aus, "
          f"um sicherzustellen, dass die neuste Version des Setup scripts ausgeführt wurde.")
    print(f'Um mit dem Service zu interagieren, nutze den'
          f'`sudo python3 {cli_args["install_location"]}/scripts/ServiceManager.py` Befehl.')
