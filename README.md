# Code zur Messdatenerfassung an Zug Radkästen

Dieses Repository beinhaltet den Code zum Aufzeichnen der Messdaten.

## Hintergrund/Motivation

Dieser Code wurde für das Projekt "Messdatenerfassung an Zug Radkästen" entwickelt.
Ziel ist es Beschleunigungsdaten am Radkasten von Zügen aufzuzeichnen.
Die Daten sollen dann für die Analyse von Schwingungen und Vibrationen verwendet werden.
Der Code wurde entwickelt, um die Daten von verschiedenen Sensoren aufzuzeichnen.
Die Sensoren können über verschiedene Schnittstellen angeschlossen werden.
Es wird ein Raspberry Pi als Hauptrechner verwendet, an dem die Sensoren angeschlossen sind und der die Daten aufzeichnet.
Der Code ist so entwickelt, dass er möglichst portabel ist und einfach weiterentwickelt werden kann.

## Technischer Überblick

Der Code ist in Python geschrieben und modular und parallelisiert aufgebaut.
Alle Sensoren lesen ihre Daten in eigenen Threads aus und schreiben sie in eine Queue.
Ein weiterer Thread liest die Daten aus der Queue und schreibt sie in eine SQLite Datenbank.
Es wird SQLite verwendet, da die Daten sehr komprimiert abgespeichert werden können und die Datenbanken sehr einfach zu handhaben sind.
Die Datenbanken werden täglich erstellt, um die Datenmenge pro Datei zu begrenzen.

Eine gängige Alternative zu einer SQLite Datenbank wären CSV Dateien.
Diese benötigen jedoch mehr Speicherplatz und sind langsamer zu lesen und zu schreiben.
Außerdem sind sie nicht so einfach zu verwalten wie SQLite Datenbanken und das Verknüpfen von Daten aus verschiedenen Dateien ist schwieriger.

Durch den neuen Aufbau des Codes gibt es auch eine neue Datenbankstruktur.
Damit die Sensoren gleichzeitig ausgelesen werden können, wird für jeden Sensor eine eigene Tabelle in der Datenbank erstellt.
Jeder Datenpunkt in der Tabelle hat einen Zeitstempel, der in Unix Zeit angegeben ist.
So können die Daten nachträglich einfach zusammengeführt werden.

## Mögliche Erweiterungen

### Sensoren

Die einfachste Weise den Code zu erweitern ist die Implementierung neuer Sensoren.
Dazu muss eine neue Klasse erstellt werden, die von der Klasse `SensorBase` erbt.
Außerdem muss eine Konfigurationsklasse erstellt werden, die von der Klasse `SensorConfig` erbt.
Zu guter Letzt muss eine Klasse erstellt werden, die von der Klasse `SensorData` erbt und die Sensorspezifischen Datenfelder und -typen enthält.
Als Referenz können die Klassen `GPS`, `ADXL357` und `MPU60X0` verwendet werden.
Diese implementieren verschiedene Schnittstellen und Sensoren und bieten eine gute Übersicht über die verschiedenen Möglichkeiten einen Sensor zu implementieren.

### Besseres Auslesen der Sensoren

Aktuell werden beim ADXL357 Sensor die Daten aus den Messregistern ausgelesen.
Dies kann zu Problemen führen, wenn die Daten zu schnell über SPI ausgelesen werden.
Um dies zu verhindern, kann der Sensor auch im FIFO Modus betrieben werden.
Es gibt ein FIFO Register, aus dem man die Daten auslesen kann.
Allerdings muss hierbei beachtet werden, dass man nicht mehr alle Daten über ein Blocklesen auslesen kann.
Ein Vorteil hingegen ist, dass es ein FIFO Statusregister gibt.
Hier kann man sehen, ob ein vollständiger Messwert im FIFO Register ist und kann so doppelte Messwerte vermeiden.

### Update auf neuere Python Versionen

Aktuell wird python 3.12 verwendet.
In dieser Version ist noch ein GIL eingeschaltet, sodass der Code nicht parallelisiert wird.
Als Teil von python 3.13 kann cPython auf eine Weise kompiliert werden, die das GIL entfernt.
So können die Standard-Threads von python verwendet werden, um den Code zu parallelisieren.
Aktuell wird der Code mit `multiprocessing` parallelisiert, was zu einem erhöhten Speicherverbrauch führt.
Außerdem ist es schwieriger Daten zwischen den Prozessen auszutauschen.
Eine Verwendung von Threads würde diese Probleme lösen und den Code effizienter machen.
Dies funktioniert allerdings nur auf python 3.13 und höher mit einer cPython Version, die ohne GIL kompiliert wurde.
Siehe [Free threaded python](https://docs.python.org/3.13/whatsnew/3.13.html#free-threaded-cpython) für mehr details.

### Speichermethoden

Aktuell werden die Daten in SQLite Datenbanken gespeichert.
Prinzipiell ist es jedoch denkbar, die Daten auch in anderen Formaten zu speichern.
Der jeweilige Adapter muss nur eine Pipe Verbindung für jeden Sensor bereitstellung und die Daten darüber entgegennehmen.
Die Daten werden in relativ generischen Dicts übergeben, sodass die Daten relativ einfach in eine optimale Form gebracht werden können.

#### CSV

Eine mögliche Alternative zu SQLite sind CSV Dateien.
Wie Anfangs erwähnt sind CSV Dateien potenziell langsamer und benötigen mehr Speicherplatz.
Allerdings sind sie einfacher zu handhaben und können von vielen Programmen gelesen werden.
Außerdem sind sie einfacher zu bearbeiten, da dies in einem Texteditor oder Tabellenkalkulationsprogramm erfolgen kann.
Die Implementierung einer CSV Speichermethode wäre relativ einfach, da die Daten bereits in Tabellenform vorliegen.
Hier würde dann eine CSV Datei pro Sensor erstellt, die die Daten enthält.
So kann das aktuelle Sensordatenformat beibehalten werden und es kommt zu keinen größeren Änderungen im Code.

#### JSON

Eine weitere Möglichkeit wäre die Daten in JSON Dateien zu speichern.
JSON ist ein sehr verbreitetes Format und kann von vielen Programmiersprachen gelesen werden.
Allerdings ist es nicht so einfach neue Messpunkte hinzu zu fügen, da die Dateien nicht in Tabellenform vorliegen.
Die Implementierung einer JSON Speichermethode wäre relativ einfach, da die Daten bereits in einem Dictionary vorliegen.
Hier würde dann eine JSON Datei pro Sensor erstellt, die die Daten enthält.

#### InfluxDB

InfluxDB ist eine Zeitreihendatenbank, die speziell für Zeitreihendaten entwickelt wurde.
Sie ist sehr performant und kann große Datenmengen speichern.
Außerdem bietet sie eine einfache Abfragesprache, die speziell für Zeitreihendaten entwickelt wurde.
InfluxDB kann auch sehr einfach mit Grafana kombiniert werden, um die Daten zu visualisieren.
Hierbei muss allerdings beachtet werden, dass InfluxDB mehr Ressourcen benötigt als SQLite, da es ein volles Datenbankmanagementsystem ist, was im Hintergrund läuft.
Wenn also die Ressourcen des Raspberry Pi ausgeschöpft sind, könnte InfluxDB zu viel sein.
Allerdings wäre es auch möglich, InfluxDB auf einem anderen Rechner zu betreiben und die Daten von dort abzufragen.
Dies erfordert jedoch eine relativ stabile Netzwerkverbindung und einen Server, der die Datenbank hostet.
Die Implementierung von InfluxDB sollte sehr ähnlich zu SQLite sein, da InfluxDB auch eine SQL ähnliche Abfragesprache verwendet.
So müsste nur eine InfluxDB Klasse erstellt werden, die die Daten in InfluxDB speichert.

#### Andere Datenbanken (SQL)

Prinzipiell sind auch andere Datenbanken denkbar, zumindest solange der Aufbau um eine Internetverbindung erweitert wird.
Die meisten Datenbankmanagementsysteme verwenden eine SQL ähnliche Abfragesprache, sodass die Implementierung relativ einfach sein sollte.
Diese sollten aber auf einem anderen Rechner laufen, da es zu Ressourcenproblemen auf dem Raspberry Pi kommen könnte.
Ins besondere der Prozessor könnte zu langsam sein, bzw. zu wenig Kerne haben, um die Datenbank effizient zu betreiben zusätzlich zu den Sensoren.
Prinzipiell ist es möglich die Datenbanken auch auf dem Raspberry Pi zu betreiben kann aber problematisch werden, wenn die Datenbank zu groß werden oder viele Sensoren angeschlossen sind.
Wie für InfluxDB müsste dann eine neue Klasse erstellt werden, die die Daten in der neuen Datenbank speichert.
Für die Implmentierung kann man sich hier auch wieder bei der SQLite Klasse orientieren.

## Verwendung als Service

### Installation

```bash
sudo apt install python3.12 wget
wget "https://git.thm.de/messdaten-radkasten/code-messdatenerfassung/-/raw/main/scripts/Setup.py" --quiet -O - | sudo python3.12 -
sudo python3 /opt/radkasten/scripts/ServiceManager.py pipenv_setup
```

Diese Befehle installieren das Programm und alle benötigten Abhängigkeiten.
Es wird auch der systemd Service (radkasten.service) erstellt, der das Programm automatisch startet.
Bevor der Service gestartet wird, sollte die Konfiguration in der Datei `/etc/radkasten/config.json` angepasst werden.
Für Details zur Konfiguration und zum Datenbankaufbau siehe weiter unten.
Standardmäßig wird der Service in `/opt/radkasten` installiert.
Die Datenbanken werden dann in `/opt/radkasten/dataBases` gespeichert.

### Interaktion mit dem Service

Für die Interaktion mit dem Service gibt es ein Hilfsprogramm, das die wichtigsten Funktionen bereitstellt.
Dies ist der folgende Befehl:

```bash
sudo python3 /opt/radkasten/scripts/ServiceManager.py [command]
```

Es gibt die folgenden Befehle:

* `--help`: Zeigt die Hilfe an
* `start`: Startet den Service
* `stop`: Stoppt den Service
* `enable`: Aktiviert den Service, sodass er beim Start des Systems automatisch gestartet wird
* `disable`: Deaktiviert den Service, sodass er beim Start des Systems nicht automatisch gestartet wird
* `restart`: Startet den Service neu
* `status`: Zeigt den Status des Services an
* `journal`: Zeigt das Journal des Services an (detaillierter log)
* `update`: Updated das Repo, pipenv und die Service Dateien
* `pipenv_setup`: Installiert die pip dependencies über pipenv

### Service Debugging

Manchmal müssen weitere analysen durchgeführt werden, um Fehler zu finden.
Hierfür ist es oft hilfreich das Messprogramm manuell zu starten.

Vor dem manuellen Starten des Programms sollte der Service gestoppt werden.
Dies kann mit dem folgenden Befehl gemacht werden:

```bash
sudo python3 /opt/radkasten/scripts/ServiceManager.py stop
```

Das manuelle Starten des Programms erfolgt dann mit dem folgenden Befehl:

```bash
cd /opt/radkasten
sudo pipenv run main --config /etc/radkasten/config.json
```

Einfache Fehler sind oft schon auf der Konsole sichtbar oder in den Logging Dateien.
Die Logging Dateien sind in `/opt/radkasten/logs` zu finden.

Wenn das Script manuell im Terminal gestartet wurde, kann es auch mit `Ctrl+C` beendet werden.
Es reicht einmal `Ctrl+C` zu drücken, da das Programm dann sauber beendet wird.
Dies kann eine Weile dauern, da das Programm auf das Beenden der Threads wartet.

Für bessere Debugging Informationen kann das Programm mit einem Profiler gestartet werden.

```bash
cd /opt/radkasten
sudo pipenv run main --config /etc/radkasten/config.json --profile
```

Dieser Befehll erstellt zwei Dateien im Ordner `/opt/radkasten`.
Eine Datei hat die Endung `.callgrind` und die andere `.pstats`.
Beide Dateien können mit verschiedenen Tools geöffnet werden, um die Performance des Programms zu analysieren.
Diese Dateien beinhalten Informationen über die Laufzeit der Funktionen und wie oft sie aufgerufen wurden.
So kann nachvollzogen werden, welche Funktionen am meisten Zeit benötigen und wo Optimierungen vorgenommen werden können.

Die `.callgrind` Datei kann mit `kcachegrind` geöffnet werden.

Beide Dateien können mit `gprof2dot` zu einer dot Datei umgewandelt werden.
Diese kann dann mit `dot` zu einem Graphen gerendert werden oder mit `xdot` angezeigt werden.
Beispiel:

```bash
# Öffne die callgrind Datei mit kcachegrind
kcachegrind ./profile_funcs_2024-06-18-17-25-19.callgrind

# Erstellt die dot datei aus der callgrind Datei
pipenv run gprof2dot -f callgrind ./profile_funcs_2024-06-18-17-25-19.callgrind > callgrind.dot

# Zeigt die dot Datei interaktiv an
pipenv run xdot callgrind.dot

# Erstellt direkt eine svg Datei aus der pstats Datei
pipenv run gprof2dot -f pstats ./profile_funcs_2024-06-18-17-25-19.pstats | dot -Tsvg > pstats.svg
```

## Standalone Verwendung

### Installation

```bash
sudo apt install python3.12 pipenv git
git clone https://git.thm.de/messdaten-radkasten/code-messdatenerfassung.git
cd code-messdatenerfassung
pipenv install --python python3.12
python3 ./scripts/Setup.py --local_install
```

Dies installiert das Programm und alle benötigten Abhängigkeiten.

### Ausführen

Das Programm kann mit `pipenv run python3 main.py` oder `pipenv run ./main.py` ausgeführt werden.

Es gibt einige optionale Argumente:

- --help: Zeigt die Hilfe an
- --config: Der Pfad zur Konfigurationsdatei (Standard: config.json)
- --log: Das Log Level für die Terminal-ausgabe (Standard: INFO)

## Datenbanken

Alle Messdaten werden in SQLite Datenbanken gespeichert.
Die Datenbanken werden im Ordner `dataBases` gespeichert.
Die Datenbanken werden nach dem Schema `dataYYYY-MM-DD.db` benannt.
Das heißt, dass für jeden Tag eine neue Datenbank erstellt wird.
Dies hat den Vorteil, dass die Datenbanken nicht zu groß werden und die Daten leichter zu verwalten sind.
Mit externen Tools können die Datenbanken dann zusammengeführt werden.

Für einfaches anschauen und bearbeiten der Datenbanken kann das Tool `DB Browser for SQLite` verwendet werden.
Dieses ist kostenlos und Open Source und kann von [sqlitebrowser.org](https://sqlitebrowser.org/) heruntergeladen werden.

### Tabellen

Anders als zuvor gibt es jetzt für jeden Sensortyp eine eigene Tabelle.
Die Tabellen sind nach dem Schema `type_data` benannt.
Aktuell sind die folgenden Tabellen vorhanden:

- `gps_data`: Enthält die GPS Daten
- `adxl357_data`: Enthält die Daten von Sensoren des Typs ADXL357
- `mpu60x0_data`: Enthält die Daten von Sensoren des Typs MPU6050 oder MPU6000

Jeder Wert in den Tabellen hat einen Zeitstempel, der in Unix Zeit angegeben ist.
Dieser Zeitstempel ist der Zeitpunkt, an dem der Wert aufgezeichnet wurde.
GPS Daten haben zusätzlich noch einen Zeitstempel, der von GPSD geliefert wird.

**Wichtig**: Die Tabellen werden nur erstellt, wenn auch ein Sensor dieses Typs vorhanden ist.
Das heißt, dass wenn kein GPS Sensor vorhanden ist, wird auch keine `gps_data` Tabelle erstellt.
Außerdem können die Tabellennamen in der Konfiguration geändert werden.
So kann man sicherstellen, dass verschiedene Sensoren vom gleichen Typ in verschiedenen Tabellen gespeichert werden.

## Konfiguration

Die Konfiguration erfolgt über die Datei `config.json`.
Diese wird nach dem ersten Start des Programms erstellt.
Die Konfiguration kann dann angepasst werden.
In der Konfiguration können die Sensoren konfiguriert werden.
Sowohl die Sensoren als auch die Konfiguration selbst sind in der Datei [config_options.md](docs/config_options.md) beschrieben.
Für eine Beispielkonfiguration mit allen Sensoren siehe [example_config.json](example_config.json).

**Wichtig**: Das Programm startet nicht, wenn die Konfiguration nicht korrekt ist.
Es wird eine Fehlermeldung ausgegeben, die beschreibt, was falsch ist.

### Beispiel Konfiguration und Aufbau

In diesem Beispiel wird ein GPS Sensor und ein ADXL357 Sensor konfiguriert.
Der AXDL357 Sensor ist über I2C an den Bus 1 angeschlossen und hat die Adresse 0x1D.
Der Messbereich des Sensors beträgt 40g.

Die GPS Daten werden 10 mal pro Sekunde abgefragt.
Die Antenne ist in diesem Fall über USB angeschlossen.
GPSD sollte automatisch erkennen, dass ein GPS Gerät angeschlossen ist und wo es angeschlossen ist.
Somit sollte keine weitere Konfiguration nötig sein.

Im folgenden Bild ist der Aufbau des Beispiels zu sehen.
Der SDA Pin des Sensors ist an Pin 3 des Raspberry Pi angeschlossen und der SCL Pin ist an Pin 5 angeschlossen.
Dies sind die Standard Pins für I2C auf dem Raspberry Pi für den I2C Bus 1.
Für GND und VCC können die Pins 6 und 2 verwendet werden.

![Bild Beispiel Aufbau](docs/Beispiel_Aufbau.jpg)

Hier ist die verwendete Konfigurationsdatei.
Der folgende json Text muss in der Datei `config.json` gespeichert werden, damit die Konfiguration funktioniert.

```json
{
    "log_values": false,
    "log_discarded": false,
    "version": "1.0.0",
    "dbConfig": {
        "append_date": true,
        "base_name": "data",
        "directory": "dataBases",
        "worker_sleep": 0.1,
        "worker_mutex_timeout": 1,
        "backup_interval_m": 60,
        "backup_max_files": 10,
        "backup_subdir": "backup"
    },
    "sensorConfig": [
        {
            "type": "GPS",
            "timeout": 60,
            "polling_rate": 10
        },
        {
            "type": "ADXL357",
            "m_range": 40,
            "no_temp": false,
            "poll_sleep": 0.1,

            "connectionType": "I2C",
            "bus_id": 1,
            "address": "0x1D",
            "measurement_packets": 2,

            "high_speed": false
        }

    ]
}

```

## Systemuhrzeit

Der Code verwendet die Systemuhrzeit, um die Zeitstempel für die Messdaten zu erstellen.
Es ist daher wichtig, dass die Systemuhrzeit korrekt ist, was normalerweise durch NTP erreicht wird.
Da der Messaufbau jedoch oft an Orten ohne Internetverbindung stattfindet, muss das System anders synchronisiert werden.
Standardmäßig wird unter den meisten Linux Distributionen die Systemuhrzeit mit systemd-timesyncd synchronisiert.
Dieser Dienst kann jedoch nicht ohne Internetverbindung verwendet werden.
Deswegen wird empfohlen chrony zu verwenden.
Anders als die meisten anderen NTP Clients kann chrony auch ohne Internetverbindung verwendet werden.
Die zusätzlich benötigte Software kann mit dem Befehl `sudo apt install chrony fake-hwclock` installiert werden.

### chrony Konfiguration

Nach der Installation von chrony muss die Konfiguration angepasst werden.
Standardmäßig ist die Konfiguration so eingestellt, dass chrony die Systemuhrzeit mit einem NTP Server synchronisiert.
Da dies nicht möglich ist, muss die Konfiguration angepasst werden.
Der folgende Befehl fügt eine Konfigurationsdatei für gpsd hinzu, die die Systemuhrzeit mit einem GPS Modul synchronisiert.

```bash
echo -e "# set larger delay to allow the NMEA source to overlap with\n# the other sources and avoid the falseticker status\nrefclock SHM 0 refid GPS precision 1e-1 offset 0.9999 delay 0.2\nrefclock SHM 1 refid PPS precision 1e-7" | sudo tee /etc/chrony/conf.d/gpsd.conf 
```

Für mehr details schau in der Dokumentation von chrony und gpsd nach.
Als Teil der installaierten Software wird auch fake-hwclock installiert.
Dieser speichert die Systemzeit beim Herunterfahren und lädt sie beim Starten wieder.
Dies verhindert, dass die Systemzeit auf den 1. Januar 1970 zurückgesetzt wird, wenn keine Internetverbindung vorhanden ist.
In Kombination mit der GPS Uhrzeit sollte die Systemzeit dann korrekt sein.
So kann man sich ein zusätzliches RTC Modul sparen.
Fake-hwclock funktioniert in Kombination mit chrony und gpsd, solange das System nicht länger als 19,7 Jahre ausgeschaltet ist.
In diesem Fall kann nicht unterschieden werden, in welchem GPS Zeitfenster sich die Systemzeit befindet.
Da dieses Szenario jedoch sehr unwahrscheinlich ist, sollte dies kein Problem darstellen und viele RTC Module haben eine
ähnliche Lebensdauer und müssen dann auch ersetzt werden und die Zeit über das Netzwerk synchronisiert werden.

## Hilfsscripts

Als Teil der Codebase gibt es auch ein paar Scripts die, die Interaktion mit dem Programm und den Daten erleichtern.
Diese sind im Ordner `scripts` zu finden und müssen ebenfalls über pipenv gestartet werden.
Die Scripts sind:

* `ServiceManager.py`: Ein Script um den Service zu starten, stoppen, neuzustarten, den Status anzuzeigen und das Journal anzuzeigen.
* `Setup.py`: Ein Script um das Programm zu installieren bzw. upzudaten.
* `DBConverter.py`: Ein Script um die Datenbanken in das alte Datenbankenformat umzuwandeln.
* `SimplePlot.py`: Ein Script um die Daten aus einer Datenbank zu plotten.
* `PlotGUI.py`: Eine GTK GUI, für nutzerfreundliches Plotten der Daten (nutzt `SimplePlot.py`).

Mit dem Befehlt `pipenv scripts` kann die Liste aller Scripts angezeigt werden.
Dies beinhaltet auch das Hauptscript `main.py` und die unittests.
Die scripts können dann mit `pipenv run SCRIPT_COMMAND` ausgeführt werden.

Hinweis:
Das ServiceManager- und Setup-Script müssen nicht über pipenv gestartet werden, da sie keine Abhängigkeiten haben.

## Entwicklung

Für die Entwicklung ist zu beachten, dass vor einem Commit aufs git der Code formatiert werden sollte.
Hierfür wird der Befehl `pipenv run python3 -m yapf -i **.py` verwendet.
Dieser formatiert alle .py Dateien im aktuellen Verzeichnis und seinen Unterverzeichnissen.
Die Formatierung erfolgt nach den Vorgaben in der Datei `.style.yapf`.

## Dokumentation

Um die Dokumentation zu generieren werden weitere dependencies benötigt.

```bash
sudo apt install doxygen dot wget unzip
```

Die Dokumentation kann dann mit dem Befehl `./build_docs.sh` generiert werden.
Im Ordner `doc_output` befindet sich dann die generierte Dokumentation.

