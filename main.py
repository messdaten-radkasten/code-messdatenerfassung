#!/usr/bin/env python3

import os.path
import signal
import time
import argparse
import logging
import psutil
import yappi
import datetime
from typing import List

from utils import DBConnector, config, LoggingInterface, I2CConnector
from sensors.SensorLoader import SensorLoader
from sensors.base import SensorBase

# import the default sensors to make sure the SensorLoader can find them
import sensors.implementations.GPSReader
import sensors.implementations.ADXL357
import sensors.implementations.MPU60X0


is_profiling = False


class Main:
    SensorList: List[SensorBase] = []
    main_logger: logging.Logger = logging.getLogger('Main')

    def __init__(self, conf: config):
        self.db = DBConnector.DBConnector(conf['dbConfig'])
        log_discarded = conf['log_discarded']
        log_values = conf['log_values']

        try:
            for sensor in conf['sensorConfig']:
                self.SensorList.append(
                    SensorLoader.load_sensor(
                        json_sensor_config=sensor, database=self.db, log_discarded=log_discarded, log_values=log_values
                    )
                )
        except Exception as e:
            self.main_logger.exception('Error loading sensors', exc_info=e)
            self.cleanup(0, 0)
            exit(1)

    def __del__(self):
        self.cleanup(0, 0)

    def run(self):
        signal.signal(signal.SIGINT, self.cleanup)
        signal.signal(signal.SIGTERM, self.cleanup)

        try:
            while psutil.disk_usage(".").percent < 99:
                time.sleep(10)
        except KeyboardInterrupt:
            self.cleanup(0, 0)
            pass

    def cleanup(self, signum, frame):
        self.main_logger.info('Ctrl+C detected, exiting...')

        for sensor in self.SensorList:
            sensor.stop_polling()

        self.SensorList.clear()

        I2CConnector.I2CConnector.stop_all()

        if self.db is not None:
            self.main_logger.info('Closing database connection')
            self.db.close()
            self.db.__del__()
            self.db = None
            self.main_logger.info('Database connection closed')

        if is_profiling:
            logging.warning('Stopping yappi profiler')
            yappi.stop()

            timestamp = datetime.datetime.now().strftime('%Y-%m-%d-%H-%M-%S')

            func_stats = yappi.get_func_stats()
            func_stats.save(f"profile_funcs_{timestamp}.callgrind", type='callgrind')
            func_stats.save(f"profile_funcs_{timestamp}.pstats", type='pstat')

            yappi.get_thread_stats().print_all()
            yappi.clear_stats()

        if signum != 0 or frame != 0:
            exit(0)


if __name__ == "__main__":
    main_script_dir = os.path.dirname(__file__)
    defaultConfigPath: os.path = os.path.join(main_script_dir, 'config.json')

    parser = argparse.ArgumentParser(
        prog='messdatenerfassung', description='Sammelt Daten von Sensoren und speichert sie in einer Datenbank'
    )

    parser.add_argument(
        '--config',
        dest='config_file',
        action='store',
        default=defaultConfigPath,
        help='Wähle die config Datei aus, die die Sensoren konfiguriert.'
        'Wenn keine Datei angegeben wird, wird die Datei config.json erstellt und verwendet.'
    )

    parser.add_argument(
        '--log',
        dest='logging_level',
        action='store',
        default='INFO',
        choices=['DEBUG', 'VALUE', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
        help='Wähle das logging level für die Konsolenausgabe aus.'
    )

    parser.add_argument(
        '--profile',
        dest='profile',
        action='store_true',
        default=False,
        help='Startet das Programm mit dem yappi Profiler.'
    )

    cli_args = vars(parser.parse_args())

    # set up logging
    LoggingInterface.setup_logging(main_dir=main_script_dir, log_level=cli_args['logging_level'])

    is_profiling = cli_args['profile']
    if is_profiling:
        logging.warning('Starting yappi profiler')
        yappi.start()

    # try loading the config
    try:
        mainconf = config.config(cli_args['config_file'])
    except Exception as e:
        logging.exception('Error loading configuration', exc_info=e)
        exit(1)

    # create the main program which loads all sensors from the config
    mainProgram = Main(mainconf)

    # go into the main program loop
    mainProgram.run()
