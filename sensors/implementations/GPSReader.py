import shutil
import subprocess
from sensors.base.SensorBase import SensorBase
from sensors.base.SensorData import SensorData, SensorDataField, DefaultFields
from gpsdclient import GPSDClient

from sensors.SensorLoader import SensorLoader

## \namespace sensors.implementations.GPSReader
# Dieses Modul beinhaltet die Implementierung des GPS-Sensors.
# Zur Kommunikation mit dem/den GPS-Sensor(en) wird GPSD verwendet.
# So kann diese Klasse mit einer Vielzahl von GPS-Sensoren kommunizieren, ohne hardware-spezifischen Code zu verwenden.
# Die GPSD-Client-Bibliothek wird verwendet, um GPS-Daten von GPSD zu lesen.
# Wichtig ist, dass GPSD auf dem System installiert und ausgeführt wird, bevor diese Klasse verwendet wird.


class GPSData(SensorData):
    """Die GPS-Datenstruktur.
    GPS-Daten haben die folgenden zusätzlichen Felder:
    - gps_time: Die Zeit, die vom GPS-Sensor erfasst wurde
    - latitude: Die Breitengradkoordinate
    - longitude: Die Längengradkoordinate
    - altitude: Die Höhe über dem Meeresspiegel
    """

    @staticmethod
    def get_fields():
        """Gibt die Standardfelder für GPS-Daten zurück.
        Die zusätzlichen Felder für GPS-Daten sind:
        - gps_time: Die Zeit, die vom GPS-Sensor erfasst wurde
        - latitude: Die Breitengradkoordinate
        - longitude: Die Längengradkoordinate
        - altitude: Die Höhe über dem Meeresspiegel
        Für mehr Informationen siehe sensors.base.SensorData.SensorData.get_fields

        :return: Die Standardfelder für GPS-Daten
        """
        return DefaultFields.default_fields() + [
            SensorDataField('gps_time', 'REAL'),
            SensorDataField('latitude', 'REAL'),
            SensorDataField('longitude', 'REAL'),
            SensorDataField('altitude', 'REAL')
        ]

    def __init__(self, table_name: str = 'gps_data', **kwargs):
        """Initialisiert die GPS-Daten.
        Der standardmäßige Tabellenname für GPS-Daten ist 'gps_data'.

        :param table_name: Der Name der Tabelle, in der die GPS-Daten gespeichert werden sollen
        :param kwargs: Die restlichen Schlüsselwortargumente, die an die Elternklasse übergeben werden
        """
        super().__init__(table_name=table_name, **kwargs)


class GPSReader(SensorBase):
    """Die GPS-Sensorimplementierung.
    Diese Klasse liest die GPS-Daten von GPS-Sensoren mit GPSD und übergibt sie an den DBConnector.

    Attributes:
        client: Der GPSD-Client, der verwendet wird, um GPS-Daten von GPSD zu lesen

        _timeout: Die Zeit in Sekunden, die gewartet wird, bis die Verbindung zum GPS-Sensor abbricht
        _polling_rate: Die Rate, mit der der GPS-Sensor abgefragt wird
    """

    def __init__(self, **kwargs):
        """Initialisiert den GPS-Sensor.
        Bevor die GPSDClient-Instanz erstellt wird, wird die GPS-Polling-Rate festgelegt.
        polling_rate wird über die Kommandozeile mit gpsctl festgelegt.
        Der hierfür ausgeführte Befehl ist: `gpsctl -c (1.0 / polling_rate)`
        Dies ändert die Polling Rate von GPSD und beinflusst so auch andere Anwendungen, die GPSD verwenden.

        :param kwargs: Die Argumente für den GPS-Sensor
        """
        super().__init__(name='GPSReader', default_table_name='gps_data', **kwargs)

        self._timeout = kwargs.get('timeout', 60)

        self._polling_rate = None
        if "polling_rate" in kwargs:
            self._polling_rate = float(kwargs.get('polling_rate'))

        if shutil.which("gpsd") is None:
            self._print_err("gpsd is not installed. Please install gpsd to use the GPS sensor.")
            return

        if self._polling_rate is not None:
            if shutil.which('gpsctl') is None:
                self._print_err('gpsctl is not installed. Please install gpsctl to set the GPS polling rate.')
            else:
                self._print_info('Set the GPS polling rate')

                cycle_time = 1.0 / self._polling_rate
                try:
                    subprocess.call(['gpsctl', '-c', str(cycle_time)])
                except Exception as e:
                    self._print_err('Failed to set the GPS polling rate:', e)

        self._print_dbg('Connecting to GPSD')

        self.client = GPSDClient(timeout=float(self._timeout))

        self._print_dbg('Connected to GPSD')

        # Start polling the GPS sensor
        self._start_polling()

    def _poll_and_record(self):
        """Fragt den GPSD-Client nach GPS-Daten ab und reicht sie an den DBConnector weiter."""

        self._print_dbg('Polling GPSD for GPS data')

        # Poll the GPSD client for GPS data
        # Only read TPV (Time, Position, Velocity) data
        # The GPSD client will return a dictionary with the GPS data
        # Incomplete data will be ignored
        for result in self.client.dict_stream(convert_datetime=True, filter=["TPV"]):
            # If the sensor should stop polling, stop polling
            if not self._isPolling:
                break

            if not result:
                continue

            # check https://gpsd.gitlab.io/gpsd/gpsd_json.html for the full list of fields
            if "status" in result:
                self._print_info('GPSD status:', result["status"])

            if not self._require_fields(["lat", "lon", "alt", "time"], result, "TPV data point"):
                if SensorBase._gps_location_found:
                    continue
                else:
                    self._print_info('Got incomplete GPS Data waiting for more...')
                    self._print_val('Raw GPS data:', result)
                    continue

            ts = result["time"].timestamp()
            lat = result["lat"]
            lon = result["lon"]
            alt = result["alt"]
            port = 'GPSD://' + result.get("device", "unknown")

            try:
                gps_data = GPSData(
                    sensor_name=self._name,
                    gps_time=ts,
                    latitude=lat,
                    longitude=lon,
                    altitude=alt,
                    port=port,
                    table_name=self._table_name
                )
            except Exception as e:
                self._print_err('Failed to create GPS data:', e)
                continue

            self._print_val('Recorded GPS data:', gps_data)
            self._print_val('Full GPS data:', result, is_discarded=True)

            # Record the GPS data to the database
            self._db_conn.send(gps_data)

            # mark the GPS location as found, so all the sensors can start polling
            if not SensorBase._gps_location_found:
                SensorBase._gps_location_found = True
                self._print_info('GPS location found')


SensorLoader.register_sensor('GPS', GPSReader)
