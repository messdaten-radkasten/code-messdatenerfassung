import os
import time
from enum import StrEnum
from typing import List, Dict

import smbus2
import spidev

import sensors.base.SensorBase
from sensors.base.SensorData import SensorData, SensorDataField, DefaultFields

from utils.I2CConnector import I2CConnector, I2CMessage
import utils.NumberUtils

from sensors.SensorLoader import SensorLoader

## \namespace sensors.implementations.ADXL357
# Dieses Modul enthält die Implementierung des ADXL357 Beschleunigungssensors.
# Der ADXL357 ist ein Beschleunigungssensor, der über den I2C oder SPI Bus angesprochen werden kann.
# Der Sensor kann Beschleunigungsdaten und Temperaturdaten liefern. Die Implementierung enthält Klassen für die
# Konfiguration, die Daten, die Initialisierung und das Polling des Sensors.


class ConnectionType(StrEnum):
    """Ein Enum für die verschiedenen Verbindungstypen des ADXL357.
    Die möglichen Werte sind 'I2C' und 'SPI'. Die Verwendung eines enum ist deutlich schneller und sicherer als
    die Verwendung von Strings.

    Attributes:
        I2C: Der I2C Verbindungstyp
        SPI: Der SPI Verbindungstyp
    """
    I2C = 'I2C'
    SPI = 'SPI'


class ADXL357Data(SensorData):
    """Eine Klasse für die Beschleunigungsdaten des ADXL357.
    Diese Klasse erbt von der Klasse AccelerometerData und fügt ein Feld für die Temperatur hinzu.
    """

    @staticmethod
    def get_fields():
        """Gibt die Felder für den ADXL357 zurück.
        Die Felder sind, zusätzlich zu den Standardfeldern, die Felder für die Beschleunigungsdaten und die Temperatur.
        Siehe \ref sensors.base.SensorData.SensorData.get_fields für weitere Informationen.
        """

        return DefaultFields.default_fields() + DefaultFields.accelerometer_fields() + [
            SensorDataField('temperature', 'REAL')
        ]

    def __init__(self, table_name: str = 'adxl357_data', **kwargs):
        """Initialisiert die Beschleunigungsdaten des ADXL357.
        Hier wird die Tabelle für die Beschleunigungsdaten festgelegt und die Temperatur hinzugefügt.
        Der standardmäßige Tabellenname ist 'adxl357_data'.

        :param table_name: Der Name der Tabelle für die Beschleunigungsdaten
        :param kwargs: Die weiteren durchgegebenen Argumente
        """

        super().__init__(table_name=table_name, **kwargs)


class ADXLBufferManager:
    """Eine Klasse für die Verwaltung der Register und das Decodieren der Daten des ADXL357.
    Diese Klasse enthält Methoden für die Verwaltung der Register und das Decodieren der Daten des ADXL357.
    Alle Methoden sind statisch, da sie keine Instanzvariablen benötigen.
    Die Attribute sollen als Konstanten betrachtet werden und sollten nicht verändert werden.

    Attributes:
        POWER_CTL_REGISTER: Die Adresse des Power Control Registers (POWER_CTL)
        RANGE_REGISTER: Die Adresse des Range Registers (Range)
        FILTER_REGISTER: Die Adresse des Filter Registers (Filter)
        RESET_REGISTER: Die Adresse des Reset Registers (Reset)
        RESET_COMMAND_VALUE: Der Wert des Reset Befehls, dieser ist konstant und beträgt 0x52
        TEMP_BEGIN: Die Adresse des ersten Temperatur Registers (TEMP1)
        XDATA_BEGIN: Die Adresse des ersten X-Achsen Registers (XDATA1)
        YDATA_BEGIN: Die Adresse des ersten Y-Achsen Registers (YDATA1)
        ZDATA_BEGIN: Die Adresse des ersten Z-Achsen Registers (ZDATA1)
    """

    POWER_CTL_REGISTER = 0x2d
    RANGE_REGISTER = 0x2c
    FILTER_REGISTER = 0x28
    RESET_REGISTER = 0x2f
    RESET_COMMAND_VALUE = 0x52
    TEMP_BEGIN = 0x06
    XDATA_BEGIN = 0x08
    YDATA_BEGIN = 0x0B
    ZDATA_BEGIN = 0x0E
    READ_BIT = 0x01
    WRITE_BIT = 0x00
    DUMMY_BYTE = 0xAA

    @staticmethod
    def get_range_value(m_range: int, high_speed: bool, active_high: bool = True) -> int:
        """Gibt den Wert für die Range Register Einstellung zurück.
        Das Range Register enthält die Einstellungen für die Messbereichseinstellung, die Unterbrechungspolarität und
        den I2C Hochgeschwindigkeitsmodus. Diese Funktion berechnet den Wert für das Range Register anhand der
        übergebenen Parameter.

        :param m_range: Die Messbereichseinstellung des ADXL357. Der Wert muss 10, 20 oder 40 sein.
        :param high_speed: Ein boolscher Wert, der angibt, ob der ADXL357 im Hochgeschwindigkeitsmodus betrieben wird.
        :param active_high: Ein boolscher Wert, der angibt, ob die Unterbrechungspolarität aktiv hoch ist.
        """
        match m_range:
            case 10:
                range_val = 0b01
            case 20:
                range_val = 0b10
            case 40:
                range_val = 0b11
            case _:
                raise ValueError('Range must be 10, 20, 40')

        interrupt_polarity = 0b1 << 6 if active_high else 0b0
        speed_mode = 0b1 << 7 if high_speed else 0b0
        return range_val | interrupt_polarity | speed_mode

    @staticmethod
    def get_power_management(no_temp: bool, data_ready_off: bool = True, standby: bool = False) -> int:
        """Gibt den Wert für das Power Management Register zurück.
        Das Power Management Register enthält die Einstellungen für den Standby Modus, die Temperaturmessung und die
        Datenbereitschaft. Diese Funktion berechnet den Wert für das Power Management Register anhand der übergebenen
        Parameter.

        :param no_temp: Ein boolscher Wert, der angibt, ob die Temperaturmessung deaktiviert ist.
        :param data_ready_off: Ein boolscher Wert, der angibt, ob die Datenbereitschaft deaktiviert ist.
        :param standby: Ein boolscher Wert, der angibt, ob der Standby Modus aktiviert ist.
        """
        standby_cfg = 0b1 if standby else 0b0
        temp_cfg = 0b10 if no_temp else 0b00
        data_ready_cfg = 0b100 if data_ready_off else 0b000

        return temp_cfg | data_ready_cfg | standby_cfg

    @staticmethod
    def get_filter_value(low_pass: int = 0b0001, high_pass: int = 0b000) -> int:
        """Gibt den Wert für das Filter Register zurück.
        Das Filter Register enthält die Einstellungen für den Hochpass- und Tiefpassfilter. Diese Funktion berechnet
        den Wert für das Filter Register anhand der übergebenen Parameter. Wofür die Werte stehen, muss dem Datenblatt
        des ADXL357 entnommen werden. Der Tiefpass ist 4 bit lang und kein Wert steht für deaktiviert. Der Hochpass
        ist 3 bit lang und 0b000 steht für deaktiviert.

        :param low_pass: Der Wert für den Tiefpassfilter. Standart ist 2000Hz und 500Hz
        :param high_pass: Der Wert für den Hochpassfilter. Standart ist deaktiviert
        """
        if high_pass not in [0b000, 0b001, 0b010, 0b011, 0b100, 0b101, 0b110]:
            raise ValueError('High pass filter must be 3 bits long and one of the supported values')

        if low_pass not in [0b0000, 0b0001, 0b0010, 0b0011, 0b0100, 0b0101, 0b0110, 0b0111, 0b1000, 0b1001, 0b1010]:
            raise ValueError('Low pass filter must be 4 bits long and one of the supported values')

        return (high_pass << 4) | low_pass

    @staticmethod
    def _decode_20bit(data: List[int]) -> float:
        """Decodiert eine 20 Bit Werte Register vom ADXL357.
        Diese Methode decodiert die 20 Bit Werte, die in den Registern des ADXL357 gespeichert sind. Die Methode
        verwendet die Methode combine_bytes aus dem Modul NumberUtils, um die 20 Bit Werte zu kombinieren und zu
        decodieren. Es werden die drei Bytes des Registers übergeben und die Methode gibt den decodierten Wert zurück.
        Die Daten sind linksbündig und im Zweierkomplement. Die Daten werden normalisiert interpretiert.

        :param data: Die drei Bytes des Registers
        """
        return utils.NumberUtils.combine_bytes(
            data=data,
            bit_count=20,
            twos_complement=True,
            scale_result=True,
            partial_byte_last=True,
            partial_byte_right=False
        )

    @staticmethod
    def decode(data_buffer: List[int], m_range: int, no_temp: bool, **data_kwargs) -> ADXL357Data:
        """Decodiert die Daten des ADXL357.
        Diese Methode decodiert die Daten des ADXL357. Die Methode verwendet die Methode _decode_20bit, um die
        Beschleunigungsdaten zu decodieren. Die Methode verwendet die Methode combine_bytes aus dem Modul NumberUtils,
        um die Temperatur zu decodieren. Die Methode gibt ein Objekt der Klasse ADXL357Data zurück.
        Wichtig: Diese Funktion liest die Werte nicht aus den Registern, sondern wandelt die Registerdaten in
        ein nutzbares Format um.

        :param data_buffer: Die ausgelesenen Daten des ADXL357
        :param m_range: Die Messbereichseinstellung des ADXL357
        :param no_temp: Ein boolscher Wert, der angibt, ob die Temperaturmessung deaktiviert ist.

        :return: Ein Objekt der Klasse ADXL357Data
        """
        if no_temp:
            temp = 0.0
            accel_index = 0
        else:
            temp = utils.NumberUtils.combine_bytes(
                data=data_buffer[0:2],
                bit_count=12,
                twos_complement=True,
                scale_result=True,
                partial_byte_last=False,
                partial_byte_right=True
            )
            temp *= 25.0
            accel_index = 2

        x = ADXLBufferManager._decode_20bit(data_buffer[accel_index:accel_index + 3]) * m_range
        y = ADXLBufferManager._decode_20bit(data_buffer[accel_index + 3:accel_index + 6]) * m_range
        z = ADXLBufferManager._decode_20bit(data_buffer[accel_index + 6:accel_index + 9]) * m_range

        return ADXL357Data(temperature=temp, accel_x=x, accel_y=y, accel_z=z, **data_kwargs)

    @staticmethod
    def spi_write_adr(adr: int) -> int:
        """Gibt die Adresse für das Schreiben über den SPI Bus zurück.
        Diese Methode gibt die Adresse für das Schreiben über den SPI Bus zurück. Die Methode setzt das Bit für das
        Schreiben und gibt die Adresse zurück.

        :param adr: Die Adresse, die geschrieben werden soll
        """
        return adr << 1 | ADXLBufferManager.WRITE_BIT

    @staticmethod
    def spi_read_adr(adr: int) -> int:
        """Gibt die Adresse für das Lesen über den SPI Bus zurück.
        Diese Methode gibt die Adresse für das Lesen über den SPI Bus zurück. Die Methode setzt das Bit für das
        Lesen und gibt die Adresse zurück.

        :param adr: Die Adresse, die gelesen werden soll
        """
        return adr << 1 | ADXLBufferManager.READ_BIT


class ADXLSetup(I2CMessage):
    """Eine Klasse für die Initialisierung des ADXL357.
    Diese Klasse erbt von der Klasse I2CMessage und initialisiert den ADXL357. Die Klasse enthält Methoden sowohl für
    die I2C als auch für die SPI Initialisierung.

    Attributes:
        address: Die I2C Adresse des ADXL357. Der Standardwert ist 0x1d.
        range_value: Die Messbereichseinstellung des ADXL357. Der Standardwert ist 40.
        filter_value: Die Filtereinstellung des ADXL357. Der Standardwert ist 0b0001 für 2000Hz und 500Hz.
        power_value: Die Einstellung des Power Management Registers des ADXL357.
    """

    def __init__(self, address: int = 0x1d, m_range: int = 40, high_speed: bool = False, no_temp: bool = False):
        """Erstellt ein neues Objekt der Klasse ADXLSetup.
        Hier weden die Standardwerte für die Messbereichseinstellung, die Filtereinstellung und die Einstellung des
        Power Management Registers festgelegt. Die Registerwerte werden anhand der übergebenen Parameter berechnet.
        und in den Attributen gespeichert.

        :param address: Die I2C Adresse des ADXL357
        :param m_range: Die Messbereichseinstellung des ADXL357
        :param high_speed: Ein boolscher Wert, der angibt, ob der ADXL357 im Hochgeschwindigkeitsmodus betrieben wird
        :param no_temp: Ein boolscher Wert, der angibt, ob die Temperaturmessung deaktiviert ist
        """

        super().__init__(repeating=False)
        self.address = address

        self.range_value = ADXLBufferManager.get_range_value(m_range, high_speed)
        self.filter_value = ADXLBufferManager.get_filter_value(low_pass=0b0001, high_pass=0b000)
        self.power_value = ADXLBufferManager.get_power_management(no_temp, standby=False)

    def execute(self, bus: smbus2.SMBus) -> Dict:
        """Setzt die Initialisierung des ADXL357 über den I2C Bus um.
        Diese Funktion wird vom I2CConnector aufgerufen, um die Initialisierung des ADXL357 über den I2C Bus umzusetzen.
        Die Funktion schreibt die Registerwerte in die entsprechenden Register des ADXL357.

        :param bus: Der I2C Bus, über den die Initialisierung umgesetzt wird
        """
        bus.write_byte_data(self.address, ADXLBufferManager.RESET_REGISTER, ADXLBufferManager.RESET_COMMAND_VALUE)
        bus.write_byte_data(self.address, ADXLBufferManager.FILTER_REGISTER, self.filter_value)
        bus.write_byte_data(self.address, ADXLBufferManager.RANGE_REGISTER, self.range_value)
        bus.write_byte_data(self.address, ADXLBufferManager.POWER_CTL_REGISTER, self.power_value)

        return {}

    def spi_setup(self, spi: spidev.SpiDev):
        """Setzt die Initialisierung des ADXL357 über den SPI Bus um.
        Diese Funktion wird vom ADXL357 aufgerufen, um die Initialisierung des ADXL357 über den SPI Bus umzusetzen.
        Die Funktion schreibt die Registerwerte in die entsprechenden Register des ADXL357.

        :param spi: Der SPI Bus, über den die Initialisierung umgesetzt wird
        """

        spi.xfer2([
            ADXLBufferManager.spi_write_adr(ADXLBufferManager.RESET_REGISTER), ADXLBufferManager.RESET_COMMAND_VALUE
        ])
        spi.xfer2([ADXLBufferManager.spi_write_adr(ADXLBufferManager.FILTER_REGISTER), self.filter_value])
        spi.xfer2([ADXLBufferManager.spi_write_adr(ADXLBufferManager.RANGE_REGISTER), self.range_value])
        spi.xfer2([ADXLBufferManager.spi_write_adr(ADXLBufferManager.POWER_CTL_REGISTER), self.power_value])


class ADXLMessage(I2CMessage):
    """Eine Klasse für die Messung des ADXL357.
    Diese Klasse erbt von der Klasse I2CMessage und führt eine Messung des ADXL357 durch. Die Klasse enthält Methoden
    sowohl für die I2C als auch für die SPI Messung. Die Klasse enthält auch Methoden für das Decodieren der Daten.

    Attributes:
        address: Die I2C Adresse des ADXL357. Der Standardwert ist 0x1d.
        sensor_name: Der Name des Sensors. Der Standardwert ist 'ADXL357'.
        table_name: Der Name der Tabelle für die Beschleunigungsdaten. Der Standardwert ist 'adxl357_data'.
        m_range: Die Messbereichseinstellung des ADXL357. Der Standardwert ist 40.
        no_temp: Ein boolscher Wert, der angibt, ob die Temperaturmessung deaktiviert ist.
    """

    def __init__(
        self,
        bus,
        address: int = 0x1d,
        sensor_name: str = 'ADXL357',
        table_name: str = 'adxl357_data',
        m_range: int = 40,
        no_temp: bool = False
    ):
        """Erstellt ein neues Objekt der Klasse ADXLMessage.
        Die I2CMessage wird als wiederholend initialisiert. Die I2C Adresse, der Name des Sensors, der Name der Tabelle
        für die Beschleunigungsdaten, die Messbereichseinstellung und der boolsche Wert für die Temperaturmessung
        werden in den Attributen gespeichert. Die SQL Strings für die Tabelle und das Einfügen der Daten werden
        ebenfalls in den Attributen gespeichert.

        :param address: Die I2C Adresse des ADXL357
        :param sensor_name: Der Name des Sensors
        :param table_name: Der Name der Tabelle für die Beschleunigungsdaten
        :param m_range: Die Messbereichseinstellung des ADXL357
        :param no_temp: Ein boolscher Wert, der angibt, ob die Temperaturmessung deaktiviert ist
        """

        super().__init__(repeating=True)
        self.bus = bus
        self.address = address
        self.sensor_name = sensor_name
        self.table_name = table_name
        self.m_range = m_range
        self.no_temp = no_temp

        self.i2c_port = f'I2C://{self.bus}:{self.address}'
        self.spi_port = f'SPI://{self.bus}:{self.address}'

    def _decode(self, data_buffer: List[int], port: str) -> ADXL357Data:
        """Decodiert die Daten des ADXL357.
        Diese Methode decodiert die Daten des ADXL357. Die Methode verwendet die Methode decode aus der Klasse
        ADXLBufferManager, um die Beschleunigungsdaten zu decodieren. Die Methode gibt ein Objekt der Klasse
        ADXL357Data zurück. Diese Methode existiert als wrapper, um den timestamp zu einem festen Zeitpunkt zu setzen.

        :param data_buffer: Die ausgelesenen Daten des ADXL357
        :param port: Der Port, über den die Daten ausgelesen wurden

        :return: Ein Objekt der Klasse ADXL357Data
        """

        timestamp = time.time()

        return ADXLBufferManager.decode(
            data_buffer,
            port=port,
            no_temp=self.no_temp,
            m_range=self.m_range,
            timestamp=timestamp,
            table_name=self.table_name,
            sensor_name=self.sensor_name
        )

    def execute(self, bus: smbus2.SMBus) -> ADXL357Data:
        """Führt eine Messung des ADXL357 über den I2C Bus durch.
        Diese Funktion wird vom I2CConnector aufgerufen, um eine Messung des ADXL357 über den I2C Bus durchzuführen.
        Die Funktion liest die Registerwerte des ADXL357 aus und decodiert die Daten.

        :param bus: Der I2C Bus, über den die Messung durchgeführt wird

        :return: Ein Objekt der Klasse ADXL357Data
        """

        try:
            # Read all registers at once beginning at TEMP1
            if self.no_temp:
                data_buffer = bus.read_i2c_block_data(self.address, ADXLBufferManager.XDATA_BEGIN, 9)
            else:
                data_buffer = bus.read_i2c_block_data(self.address, ADXLBufferManager.TEMP_BEGIN, 11)

            return self._decode(data_buffer, self.i2c_port)

        except Exception as e:
            raise IOError('Error reading ADXL357:', e)

    def spi_measure(self, spi: spidev.SpiDev) -> ADXL357Data:
        """Führt eine Messung des ADXL357 über den SPI Bus durch.
        Diese Funktion wird vom ADXL357 aufgerufen, um eine Messung des ADXL357 über den SPI Bus durchzuführen.
        Die Funktion liest die Registerwerte des ADXL357 aus und decodiert die Daten.

        :param spi: Der SPI Bus, über den die Messung durchgeführt wird

        :return: Ein Objekt der Klasse ADXL357Data
        """

        if self.no_temp:
            data_buffer = spi.xfer2(
                [ADXLBufferManager.spi_read_adr(ADXLBufferManager.XDATA_BEGIN)] +
                [ADXLBufferManager.DUMMY_BYTE] * 9
            )[1:]
        else:
            data_buffer = spi.xfer2(
                [ADXLBufferManager.spi_read_adr(ADXLBufferManager.TEMP_BEGIN)] +
                [ADXLBufferManager.DUMMY_BYTE] * 11
            )[1:]

        return self._decode(data_buffer, self.spi_port)


class ADXL357(sensors.base.SensorBase.SensorBase):
    """Die Klasse für den ADXL357 Sensor.
    Diese Klasse erbt von der Klasse SensorBase und implementiert die Methoden für die Initialisierung und das Polling
    des ADXL357. Die Klasse enthält Methoden für das Polling über den I2C und SPI Bus.

    Attributes:
        i2c_conn: Der I2C Connector für den ADXL357
        spi_conn: Der SPI Connector für den ADXL357
        measurement_msg: Das ADXLMessage Objekt für die Messung des ADXL357

        _m_range: Die Messbereichseinstellung des ADXL357. Der Standardwert ist 40.
        _no_temp: Ein boolscher Wert, der angibt, ob die Temperaturmessung deaktiviert ist.
        _poll_sleep: Die Schlafzeit zwischen den Messungen.
        _measurement_packets: Die Anzahl der Messungen, die bei jedem Polling durchgeführt werden.
        _connection_type: Der Verbindungstyp des ADXL357.
        _bus_id: Die I2C oder SPI Bus ID.
        _address: Die I2C oder SPI Adresse. Der Standardwert ist 0x1d für I2C und 1 für SPI.

        _high_speed: Ein bool, der angibt ob der ADXL357 im Hochgeschwindigkeitsmodus betrieben wird.

        _spi_speed: Die Geschwindigkeit des SPI Busses. Der Standardwert ist 10 MHz.

    """

    def __init__(self, **kwargs):
        """Erstellt ein neues Objekt der Klasse ADXL357.
        Hier wird der I2C oder SPI Connector für den ADXL357 initialisiert und die Initialisierung des ADXL357
        durchgeführt. Die Messung des ADXL357 wird ebenfalls initialisiert.

        :param kwargs: Die Konfiguration des ADXL357
        """

        super().__init__(default_table_name='adxl357_data', name='ADXL357', **kwargs)

        # generic configuration
        self._m_range = kwargs.get('m_range', 40)
        if self._m_range not in [10, 20, 40]:
            raise ValueError('Range must be 10, 20, or 40')

        self._no_temp = kwargs.get('no_temp', False)
        self._poll_sleep = kwargs.get('poll_sleep', 0.1)

        self._measurement_packets = kwargs.get('measurement_packets', 1)
        if self._measurement_packets < 1:
            raise ValueError('measurement_packets must be greater than 0')

        self._connection_type = ConnectionType(kwargs['connectionType'].upper())

        # The I2C or SPI bus id
        self._bus_id = kwargs.get('bus_id', 0)

        # The I2C or SPI address, default is 0x1d for I2C and 1 for SPI
        if self._connection_type == ConnectionType.I2C:
            conf_addr = kwargs.get('address', '0x1d')
            self._address = int(conf_addr, 16)
        elif self._connection_type.SPI:
            self._address = int(kwargs.get('address', 1))
        else:
            raise ValueError('Connection type must be I2C or SPI')

        # I2C specific configuration
        self._high_speed = kwargs.get('high_speed', False)

        # SPI specific configuration
        self._spi_speed = kwargs.get('spi_speed', 10000000)

        setup_msg = ADXLSetup(
            address=self._address,
            m_range=self._m_range,
            high_speed=self._high_speed,
            no_temp=self._no_temp
        )

        self.measurement_msg = ADXLMessage(
            bus=self._bus_id,
            address=self._address,
            sensor_name=self._name,
            table_name=self._table_name,
            m_range=self._m_range,
            no_temp=self._no_temp
        )

        match self._connection_type:
            case 'I2C':
                bus = I2CConnector.get_bus(self._bus_id)
                self.i2c_conn = bus.connect()
                self.i2c_conn.send(setup_msg)

                for _ in range(self._measurement_packets):
                    self.i2c_conn.send(self.measurement_msg)
            case 'SPI':
                self.spi_conn = spidev.SpiDev()
                self.spi_conn.open(self._bus_id, self._address)

                self.spi_conn.max_speed_hz = self._spi_speed
                self.spi_conn.mode = 0b00
                self.spi_conn.cshigh = False
                setup_msg.spi_setup(self.spi_conn)
            case _:
                raise ValueError('Invalid connection type:', self._connection_type)

        self._start_polling()

    def _poll_and_record(self):
        """Führt das Polling des ADXL357 durch und speichert die Daten.
        Diese Methode führt das Polling des ADXL357 durch und speichert die Daten in der Datenbank. Die Methode führt
        das Polling solange durch, bis das Polling beendet wird. Die Methode verwendet die Methoden poll_i2c und
        poll_spi, um das Polling durchzuführen.
        """

        self._wait_for_gps()

        while self._isPolling:
            adxl_data: List[ADXL357Data]

            match self._connection_type:
                case ConnectionType.I2C:
                    adxl_data = self._poll_i2c_data(self.i2c_conn, ADXL357Data)
                case ConnectionType.SPI:
                    adxl_data = [
                        self.measurement_msg.spi_measure(self.spi_conn)
                        for _ in range(self._measurement_packets)
                    ]
                case _:
                    continue

            for data in adxl_data:
                self._print_val('Recorded ADXL357 data:', data)
                self._db_conn.send(data)

            time.sleep(self._poll_sleep)

        if self._connection_type == ConnectionType.SPI:
            self.spi_conn.close()


SensorLoader.register_sensor('ADXL357', ADXL357)
