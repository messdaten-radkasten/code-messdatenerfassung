import os
import time
from enum import Enum
from typing import List, Dict

import smbus2
import spidev

import sensors.base.SensorBase
from sensors.base.SensorData import SensorData, SensorDataField, DefaultFields

from utils.I2CConnector import I2CConnector, I2CMessage
import utils.NumberUtils

from sensors.SensorLoader import SensorLoader

## \namespace sensors.implementations.MPU60X0
# Dieses Modul enthält die Implementierung des MPU60X0 (MPU6000 und MPU6050) Beschleunigungs- und Gyrosensors.
# Der MPU6050 ist ein Sensor, der über den I2C Bus angesprochen werden kann.
# Der MPU6000 kann auch über den SPI Bus angesprochen werden.
# Der Sensor kann Beschleunigungsdaten und Gyroskopdaten liefern. Die Implementierung enthält Klassen für die
# Konfiguration, die Daten, die Initialisierung und das Polling des Sensors.


class ConnectionType(Enum):
    """Ein Enum für die verschiedenen Verbindungstypen des MPU60X0.
    Die möglichen Werte sind 'I2C' und 'SPI'. Die Verwendung eines enum ist deutlich schneller und sicherer als
    die Verwendung von Strings.

    Attributes:
        I2C: Der I2C Verbindungstyp
        SPI: Der SPI Verbindungstyp
    """
    I2C = 'I2C'
    SPI = 'SPI'


class MPU60X0Data(SensorData):
    """Eine Klasse für die Beschleunigungsdaten des MPU60X0.
    Diese Klasse fügt alle Daten, die der Sensor aufnehmen kann hinzu.
    """

    @staticmethod
    def get_fields():
        """Gibt die Felder für den MPU60X0 zurück.
        Die Felder sind, zusätzlich zu den Standardfeldern, die Felder für die Beschleunigungsdaten, die Gyroskopdaten
        und die Temperatur.
        Siehe \ref sensors.base.SensorData.SensorData.get_fields für weitere Informationen.
        """

        return (
            DefaultFields.default_fields() + DefaultFields.accelerometer_fields() + DefaultFields.gyroscope_fields() +
            [SensorDataField('temperature', 'REAL')]
        )

    def __init__(self, table_name: str = 'mpu60X0_data', **kwargs):
        """Initialisiert die Beschleunigungsdaten des MPU60X0.
        Hier wird die Tabelle für die Beschleunigungsdaten festgelegt und die Temperatur hinzugefügt.
        Der standardmäßige Tabellenname ist 'mpu60X0_data'.

        :param table_name: Der Name der Tabelle für die Beschleunigungsdaten
        :param kwargs: Die weiteren durchgegebenen Argumente
        """

        super().__init__(table_name=table_name, **kwargs)


class MPU60X0BufferManager:
    """Eine Klasse für die Verwaltung der Register und das Decodieren der Daten des MPU60X0.
    Diese Klasse enthält Methoden für die Verwaltung der Register und das Decodieren der Daten des MPU60X0.
    Alle Methoden sind statisch, da sie keine Instanzvariablen benötigen.
    Die Attribute sollen als Konstanten betrachtet werden und sollten nicht verändert werden.

    Attributes:
        POWER_CTL_REGISTER: Die Adresse des Power Control Registers (POWER_CTL)
        RANGE_REGISTER: Die Adresse des Range Registers (Range)
        FILTER_REGISTER: Die Adresse des Filter Registers (Filter)
        RESET_REGISTER: Die Adresse des Reset Registers (Reset)
        RESET_COMMAND_VALUE: Der Wert des Reset Befehls, dieser ist konstant und beträgt 0x52
        TEMP_BEGIN: Die Adresse des ersten Temperatur Registers (TEMP_OUT_H)
        A_XDATA_BEGIN: Die Adresse des ersten X-Achsen Registers (ACCEL_XOUT_H)
        A_YDATA_BEGIN: Die Adresse des ersten Y-Achsen Registers (ACCEL_YOUT_H)
        A_ZDATA_BEGIN: Die Adresse des ersten Z-Achsen Registers (ACCEL_ZOUT_H)
        G_XDATA_BEGIN: Die Adresse des ersten X-Achsen Registers (GYRO_YOUT_H)
        G_YDATA_BEGIN: Die Adresse des ersten Y-Achsen Registers (GYRO_YOUT_H)
        G_ZDATA_BEGIN: Die Adresse des ersten Z-Achsen Registers (GYRO_YOUT_H)
    """

    CONFIG_REGISTER = 0x1A
    GYRO_CONFIG_REGISTER = 0x1B
    ACCEL_CONFIG_REGISTER = 0x1C
    USER_CTRL_REGISTER = 0x6A
    PWR_MGMT_1_REGISTER = 0x6B

    A_XDATA_BEGIN = 0x3B
    A_YDATA_BEGIN = 0x3D
    A_ZDATA_BEGIN = 0x3F
    TEMP_BEGIN = 0x41
    G_XDATA_BEGIN = 0x43
    G_YDATA_BEGIN = 0x45
    G_ZDATA_BEGIN = 0x47

    EXT_SENS_DATA_BEGIN = 0x49

    @staticmethod
    def get_a_range_value(a_range: int) -> int:
        match a_range:
            case 2:
                a_range_val = 0b00
            case 4:
                a_range_val = 0b01
            case 8:
                a_range_val = 0b10
            case 16:
                a_range_val = 0b11
            case _:
                raise ValueError('Accel Range must be 2, 4, 8, 16')
        return 0 | a_range_val << 3

    @staticmethod
    def get_g_range_value(g_range: int) -> int:
        match g_range:
            case 250:
                g_range_val = 0b00
            case 500:
                g_range_val = 0b01
            case 1000:
                g_range_val = 0b10
            case 2000:
                g_range_val = 0b11
            case _:
                raise ValueError('Gyro Range must be 250, 500, 1000, 2000')

        return 0 | g_range_val << 3

    @staticmethod
    def get_config_value(dlpf: int = 0) -> int:
        dlpf &= 0x111
        return dlpf

    @staticmethod
    def _decode_16bit(data_buffer: List[int], scale_result: bool = True) -> float:
        """Decodiert die Daten eines 16 Bit Registers.
        Diese Methode decodiert die Daten eines 16 Bit Registers. Die Methode verwendet die Methode combine_bytes aus
        dem Modul NumberUtils, um die Daten zu decodieren. Die Methode gibt den decodierten Wert zurück.
        """
        return utils.NumberUtils.combine_bytes(
            data_buffer, bit_count=16, twos_complement=True, scale_result=scale_result
        )

    @staticmethod
    def decode(data_buffer: List[int], a_range: int, g_range: int, **data_kwargs) -> MPU60X0Data:
        """Decodiert die Daten des ADXL357.
        Diese Methode decodiert die Daten des ADXL357. Die Methode verwendet die Methode _decode_20bit, um die
        Beschleunigungsdaten zu decodieren. Die Methode verwendet die Methode combine_bytes aus dem Modul NumberUtils,
        um die Temperatur zu decodieren. Die Methode gibt ein Objekt der Klasse ADXL357Data zurück.
        Wichtig: Diese Funktion liest die Werte nicht aus den Registern, sondern wandelt die Registerdaten in
        ein nutzbares Format um.

        :param data_buffer: Die ausgelesenen Daten des MPU60X0
        :param a_range: Die Accel Messbereichseinstellung des MPU60X0
        :param g_range: Die Gyro Messbereichseinstellung des MPU60X0

        :return: Ein Objekt der Klasse MPU60X0Data
        """

        a_x = MPU60X0BufferManager._decode_16bit(data_buffer[0:2]) * a_range
        a_y = MPU60X0BufferManager._decode_16bit(data_buffer[2:4]) * a_range
        a_z = MPU60X0BufferManager._decode_16bit(data_buffer[4:6]) * a_range

        temp = MPU60X0BufferManager._decode_16bit(data_buffer[6:8], scale_result=False) / 340.0 + 36.53

        g_x = MPU60X0BufferManager._decode_16bit(data_buffer[8:10]) * g_range
        g_y = MPU60X0BufferManager._decode_16bit(data_buffer[10:12]) * g_range
        g_z = MPU60X0BufferManager._decode_16bit(data_buffer[12:14]) * g_range

        return MPU60X0Data(
            temp=temp, accel_x=a_x, accel_y=a_y, accel_z=a_z, gyro_x=g_x, gyro_y=g_y, gyro_z=g_z, **data_kwargs
        )


class MPU60X0Setup(I2CMessage):
    """Eine Klasse für die Initialisierung des MPU60X0.
    Diese Klasse erbt von der Klasse I2CMessage und initialisiert den MPU60X0. Die Klasse enthält Methoden sowohl für
    die I2C als auch für die SPI Initialisierung.

    Attributes:
        address: Die I2C Adresse des MPU60X0. Der Standardwert ist 0x1d.
        a_range_val: Die Accel Register Einstellung
        g_range_val: Die Gyro Register Einstellung
        conf_val: Die Config Register Einstellung
    """

    def __init__(self, address: int = 0x1d, a_range: int = 16, g_range: int = 250):
        """Erstellt ein neues Objekt der Klasse MPU60X0Setup.
        Hier weden die Standardwerte für die Messbereichseinstellung, die Filtereinstellung und die Einstellung des
        Power Management Registers festgelegt. Die Registerwerte werden anhand der übergebenen Parameter berechnet.
        und in den Attributen gespeichert.

        :param address: Die I2C Adresse des MPU60X0
        :param a_range: Die Accel Messbereichseinstellung des MPU60X0
        :param g_range: Die Gyro Messbereichseinstellung des MPU60X0
        """

        super().__init__(repeating=False)
        self.address = address

        self.a_range_val = MPU60X0BufferManager.get_a_range_value(a_range)
        self.g_range_val = MPU60X0BufferManager.get_g_range_value(g_range)
        self.conf_val = MPU60X0BufferManager.get_config_value()

    def execute(self, bus: smbus2.SMBus) -> Dict:
        """Setzt die Initialisierung des MPU60X0 über den I2C Bus um.
        Diese Funktion wird vom I2CConnector aufgerufen, um die Initialisierung des MPU60X0 über den I2C Bus umzusetzen.
        Die Funktion schreibt die Registerwerte in die entsprechenden Register des MPU60X0.

        :param bus: Der I2C Bus, über den die Initialisierung umgesetzt wird
        """
        bus.write_byte_data(self.address, MPU60X0BufferManager.GYRO_CONFIG_REGISTER, self.g_range_val)
        bus.write_byte_data(self.address, MPU60X0BufferManager.ACCEL_CONFIG_REGISTER, self.a_range_val)
        bus.write_byte_data(self.address, MPU60X0BufferManager.CONFIG_REGISTER, self.conf_val)

        return {}

    def spi_setup(self, spi: spidev.SpiDev):
        """Setzt die Initialisierung des MPU60X0 über den SPI Bus um.
        Diese Funktion wird vom MPU60X0 aufgerufen, um die Initialisierung des MPU60X0 über den SPI Bus umzusetzen.
        Die Funktion schreibt die Registerwerte in die entsprechenden Register des MPU60X0.

        :param spi: Der SPI Bus, über den die Initialisierung umgesetzt wird
        """

        spi.xfer([MPU60X0BufferManager.GYRO_CONFIG_REGISTER, self.g_range_val])
        spi.xfer([MPU60X0BufferManager.ACCEL_CONFIG_REGISTER, self.a_range_val])
        spi.xfer([MPU60X0BufferManager.CONFIG_REGISTER, self.conf_val])


class MPU60X0Message(I2CMessage):
    """Eine Klasse für die Messung des MPU60X0.
    Diese Klasse erbt von der Klasse I2CMessage und führt eine Messung des MPU60X0 durch. Die Klasse enthält Methoden
    sowohl für die I2C als auch für die SPI Messung. Die Klasse enthält auch Methoden für das Decodieren der Daten.

    Attributes:
        address: Die I2C Adresse des MPU60X0. Der Standardwert ist 0x1d.
        sensor_name: Der Name des Sensors. Der Standardwert ist 'MPU60X0'.
        table_name: Der Name der Tabelle für die Beschleunigungsdaten. Der Standardwert ist 'mpu60x0_data'.
        a_range: Die Accel Messbereichseinstellung des MPU60X0. Der Standardwert ist 16.
        g_range: Die Gyro Messbereichseinstellung des MPU60X0. Der Standardwert ist 250.
    """

    def __init__(
        self,
        address: int = 0x1d,
        sensor_name: str = 'MPU60X0',
        table_name: str = 'mpu60x0_data',
        a_range: int = 16,
        g_range: int = 250
    ):
        """Erstellt ein neues Objekt der Klasse MPU60X0Message.
        Die I2CMessage wird als wiederholend initialisiert. Die I2C Adresse, der Name des Sensors, der Name der Tabelle
        für die Beschleunigungsdaten, die Messbereichseinstellung und der boolsche Wert für die Temperaturmessung
        werden in den Attributen gespeichert. Die SQL Strings für die Tabelle und das Einfügen der Daten werden
        ebenfalls in den Attributen gespeichert.

        :param address: Die I2C Adresse des MPU60X0
        :param sensor_name: Der Name des Sensors
        :param table_name: Der Name der Tabelle für die Beschleunigungsdaten
        :param a_range: Die Accel Messbereichseinstellung des MPU60X0. Der Standardwert ist 16.
        :param g_range: Die Gyro Messbereichseinstellung des MPU60X0. Der Standardwert ist 250.
        """

        super().__init__(repeating=True)
        self.address = address
        self.sensor_name = sensor_name
        self.table_name = table_name
        self.a_range = a_range
        self.g_range = g_range

    def _decode(self, data_buffer: List[int], port: str) -> MPU60X0Data:
        """Decodiert die Daten des MPU60X0.
        Diese Methode decodiert die Daten des MPU60X0. Die Methode verwendet die Methode decode aus der Klasse
        MPU60X0BufferManager, um die Beschleunigungsdaten zu decodieren. Die Methode gibt ein Objekt der Klasse
        MPU60X0Data zurück. Diese Methode existiert als wrapper, um den timestamp zu einem festen Zeitpunkt zu setzen.

        :param data_buffer: Die ausgelesenen Daten des MPU60X0
        :param port: Der Port, über den die Daten ausgelesen wurden

        :return: Ein Objekt der Klasse MPU60X0Data
        """

        timestamp = time.time()

        return MPU60X0BufferManager.decode(
            data_buffer,
            port=port,
            g_range=self.g_range,
            a_range=self.a_range,
            timestamp=timestamp,
            table_name=self.table_name,
            sensor_name=self.sensor_name
        )

    def execute(self, bus: smbus2.SMBus) -> MPU60X0Data:
        """Führt eine Messung des MPU60X0 über den I2C Bus durch.
        Diese Funktion wird vom I2CConnector aufgerufen, um eine Messung des MPU60X0 über den I2C Bus durchzuführen.
        Die Funktion liest die Registerwerte des MPU60X0 aus und decodiert die Daten.

        :param bus: Der I2C Bus, über den die Messung durchgeführt wird

        :return: Ein Objekt der Klasse MPU60X0Data
        """

        try:
            data_buffer = bus.read_i2c_block_data(self.address, MPU60X0BufferManager.A_XDATA_BEGIN, 14)

            bus_file = os.readlink('/proc/self/fd/%d' % bus.fd)
            port = f'I2C://{bus_file}:{self.address}'

            return self._decode(data_buffer, port)

        except Exception as e:
            raise IOError('Error reading ADXL357:', e)

    def spi_measure(self, spi: spidev.SpiDev) -> MPU60X0Data:
        """Führt eine Messung des MPU60X0 über den SPI Bus durch.
        Diese Funktion wird vom MPU60X0 aufgerufen, um eine Messung des MPU60X0 über den SPI Bus durchzuführen.
        Die Funktion liest die Registerwerte des MPU60X0 aus und decodiert die Daten.

        :param spi: Der SPI Bus, über den die Messung durchgeführt wird

        :return: Ein Objekt der Klasse MPU60X0Data
        """

        data_buffer = spi.xfer2([MPU60X0BufferManager.A_XDATA_BEGIN] + [0] * 14)

        port = f'SPI://{spi.port}:{spi.device}'
        return self._decode(data_buffer, port)


class MPU60X0(sensors.base.SensorBase.SensorBase):
    """Die Klasse für den MPU60X0 Sensor.
    Diese Klasse erbt von der Klasse SensorBase und implementiert die Methoden für die Initialisierung und das Polling
    des MPU60X0. Die Klasse enthält Methoden für das Polling über den I2C und SPI Bus.

    Attributes:
        i2c_conn: Der I2C Connector für den MPU60X0
        spi_conn: Der SPI Connector für den MPU60X0
        measurement_msg: Das ADXLMessage Objekt für die Messung des MPU60X0

        _a_range: Die Beschleunigugnsmessbereich für den MPU60X0. Der Standardwert ist 16.
        _g_range: Der Gyroskopmessbereich für den MPU60X0. Der Standardwert ist 250.
        _poll_sleep: Die Schlafzeit zwischen den Messungen.
        _measurement_packets: Die Anzahl der Messungen, die bei jedem Polling durchgeführt werden.
        _connection_type: Der Verbindungstyp des MPU60X0 (Wichtig der MPU6050 kann nur I2C)
        _bus_id: Die I2C oder SPI Bus ID.
        _address: Die I2C oder SPI Adresse. Der Standardwert ist 0x1d für I2C und 1 für SPI.

        _high_speed: Ein bool, der angibt ob der ADXL357 im Hochgeschwindigkeitsmodus betrieben wird.

        _spi_speed: Die Geschwindigkeit des SPI Busses. Der Standardwert ist 10 MHz.
    """

    def __init__(self, **kwargs):
        """Erstellt ein neues Objekt der Klasse MPU60X0.
        Hier wird der I2C oder SPI Connector für den MPU60X0 initialisiert und die Initialisierung des MPU60X0
        durchgeführt. Die Messung des MPU60X0 wird ebenfalls initialisiert.

        :param **kwargs: Die angegebenen Schlüsselwortargumente
        """

        super().__init__(name='MPU60X0', default_table_name='mpu60X0_data', **kwargs)

        # generic configuration
        self._a_range = kwargs.get('a_range', 16)
        if self._a_range not in [2, 4, 8, 16]:
            raise ValueError('Range must be 2, 4, 8 or 16')

        self._g_range = kwargs.get('a_range', 250)
        if self._g_range not in [250, 500, 1000, 2000]:
            raise ValueError('Range must be 250, 500, 1000 or 2000')

        self._poll_sleep = kwargs.get('poll_sleep', 0.1)

        self._measurement_packets = kwargs.get('measurement_packets', 1)
        if self._measurement_packets < 1:
            raise ValueError('measurement_packets must be greater than 0')

        self._connection_type = ConnectionType(kwargs['connectionType'])

        # The I2C or SPI bus id
        self._bus_id = kwargs.get('bus_id', 0)

        # The I2C or SPI address, default is 0x1d for I2C and 1 for SPI
        if self._connection_type == 'I2C':
            conf_addr = kwargs.get('address', '0x68')
            self._address = int(conf_addr, 16)
        else:
            self._address = int(kwargs.get('address', 1))

        # I2C specific configuration
        self._high_speed = kwargs.get('high_speed', False)

        # SPI specific configuration
        self._spi_speed = kwargs.get('spi_speed', 10000000)

        setup_msg = MPU60X0Setup(
            address=self._address,
            a_range=self._a_range,
            g_range=self._g_range
        )

        self.measurement_msg = MPU60X0Message(
            address=self._address,
            sensor_name=self._name,
            table_name=self._table_name,
            g_range=self._g_range,
            a_range=self._a_range
        )

        match self._connection_type:
            case 'I2C':
                bus = I2CConnector.get_bus(self._bus_id)
                self.i2c_conn = bus.connect()
                self.i2c_conn.send(setup_msg)

                for _ in range(self._measurement_packets):
                    self.i2c_conn.send(self.measurement_msg)
            case 'SPI':
                self.spi_conn = spidev.SpiDev()
                self.spi_conn.open(self._bus_id, self._address)

                self.spi_conn.max_speed_hz = self._spi_speed
                self.spi_conn.mode = 0b00
                self.spi_conn.cshigh = False
                setup_msg.spi_setup(self.spi_conn)
            case _:
                raise ValueError('Invalid connection type:', self._connection_type)

        self._start_polling()

    def _poll_and_record(self):
        """Führt das Polling des MPU60X0 durch und speichert die Daten.
        Diese Methode führt das Polling des MPU60X0 durch und speichert die Daten in der Datenbank. Die Methode führt
        das Polling solange durch, bis das Polling beendet wird. Die Methode verwendet die Methoden poll_i2c und
        poll_spi, um das Polling durchzuführen.
        """
        self._wait_for_gps()

        while self._isPolling:
            mpu_data: List[MPU60X0Data]

            match self._connection_type:
                case ConnectionType.I2C:
                    mpu_data = self._poll_i2c_data(self.i2c_conn, MPU60X0Data)
                case ConnectionType.SPI:
                    mpu_data = [
                        self.measurement_msg.spi_measure(self.spi_conn)
                        for _ in range(self._measurement_packets)
                    ]
                case _:
                    continue

            for data in mpu_data:
                self._print_val('Recorded MPU60X0 data:', data)
                self._db_conn.send(data)

            time.sleep(self._poll_sleep)

        if self._connection_type == ConnectionType.SPI:
            self.spi_conn.close()


SensorLoader.register_sensor('MPU6000', MPU60X0)
SensorLoader.register_sensor('MPU6050', MPU60X0)
