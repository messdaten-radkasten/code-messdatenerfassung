import io
import logging
import multiprocessing.connection
import threading
import time
from abc import ABC, abstractmethod
from typing import Dict


class SensorBase(ABC):
    """Die Basisklasse für die Implementierungen der Sensoren.
    Diese Klasse ist abstrakt und sollte nicht instanziiert werden. Sie stellt eine gemeinsame
    Schnittstelle für alle Sensoren bereit. Sie richtet auch den Polling-Thread ein und stellt
    einen Mutex für den Datenzugriff bereit.

    Attributes:
        _gps_location_found: Wurde die GPS-Position gefunden?

        _name: Der Name des Sensors.
        _sensor_logger: Der Logger für den Sensor.
        _isPolling: Ist der Sensor am Pollen?
        _poll_thread: Der Polling-Thread.
        _dataMutex: Der Mutex für den Datenzugriff.

        _db_conn: Die Datenbankverbindung, in die die Daten geschrieben werden sollen.
        _table_name: Der Name der Tabelle, in die die Daten geschrieben werden sollen.
        _log_values: Sollen die Sensordaten geloggt werden?
        _log_discarded: Sollen verworfene Sensordaten geloggt werden?
    """

    _gps_location_found = False

    def __init__(self, name: str = 'SensorBase', **kwargs):
        """Initialisiert die Basisklasse für einen Sensor.
        Der Konstruktor initialisiert die Attribute und den Logger für den Sensor.

        :param name: Der Name des Sensors.
        :param kwargs: Die restlichen Schlüsselwortargumente, die an die Elternklasse übergeben werden.

        Raises:
            ValueError: Wenn keine Datenbankverbindung in den Argumenten enthalten ist.
        """

        if "db" not in kwargs:
            raise ValueError('db must be in the configuration')

        self._db_conn: multiprocessing.connection.Connection = kwargs["db"]
        if not isinstance(self._db_conn, multiprocessing.connection.Connection):
            raise ValueError('db must be of type multiprocessing.connection.Connection')
        if self._db_conn is None:
            raise ValueError('db cannot be None')

        default_table_name = kwargs.get('default_table_name', 'sensor_data')
        self._table_name = kwargs.get('tableName', default_table_name)

        self._log_values = kwargs.get('log_values', True)
        self._log_discarded = kwargs.get('log_discarded', False)
        self._should_wait_for_gps = kwargs.get('wait_for_gps', True)

        self._name = name
        self._sensor_logger = logging.getLogger(name)

        self._isPolling = False
        self._poll_thread: threading.Thread = None
        self._dataMutex: threading.Lock = threading.Lock()

    def __del__(self):
        """Destruktor für die Basisklasse.
        Der Destruktor stoppt den Polling-Thread, wenn er noch läuft.
        """
        self.stop_polling()

    def _start_polling(self):
        """Startet den Polling-Thread.
        Diese Funktion startet den Polling-Thread und setzt das isPolling-Flag.
        Der Konstruktor der Kindklasse sollte diese Funktion aufrufen, um das Polling zu starten,
        sobald der Sensor bereit ist.
        Auf dem Polling-Thread wird die _poll_and_record-Methode der Kindklasse aufgerufen.
        """
        if self._isPolling:
            return

        self._print_info('Starting polling thread for ' + self._name)

        self._isPolling = True
        self._poll_thread = threading.Thread(target=self._poll_and_record)
        self._poll_thread.start()

        self._print_info('Polling thread started for ' + self._name)

    def _wait_for_gps(self):
        """Wartet bis eine GPS-Position gefunden wurde.
        Falls in der Konfiguration wait_for_gps auf False gesetzt ist, wird diese Funktion sofort beendet.
        Diese Funktion sollte in der Implementierung von _poll_and_record am Anfang der Funktion aufgerufen werden.
        """
        if not self._should_wait_for_gps:
            return

        while not SensorBase._gps_location_found and self._isPolling:
            time.sleep(0.1)

    def _poll_i2c_data(self, i2c_conn, data_type: type):
        """Liest eine Pipe Connection zum I2C Connector nach neuen Daten aus und gibt sie zurück.
        Da viele Sensoren über I2C verbunden sind und diese Funktion nicht trivial ist, ist diese Teil, der
        allgemeinen Sensor Basis Klasse. Sie liest die Pipe Connection nach neuen Daten aus und gibt sie zurück.

        :param i2c_conn: Die Pipe Connection zum I2C Connector.
        :param data_type: Der Datentyp der Daten, die empfangen werden sollen.

        Returns:
            Eine Liste von Daten, die vom I2C Connector empfangen wurden.
            Alle Elemente der Liste sind vom Typ data_type.
        """

        data = []
        while i2c_conn.poll():
            dat = i2c_conn.recv()
            if isinstance(dat, data_type):
                data.append(dat)
            elif issubclass(type(dat), Exception):
                self._print_err(dat)

        return data

    def stop_polling(self):
        """Stoppt das Polling und wartet auf das Ende des Polling-Threads.
        Diese Funktion setzt die isPolling-Flag auf False und wartet auf das Ende des Polling-Threads.
        Sie sollte aufgerufen werden, bevor die Datenbankverbindung zerstört wird.
        """
        if not self._isPolling:
            return

        self._print_info('Stopping polling for ' + self._name)
        self._isPolling = False
        self._poll_thread.join()
        self._print_info('Polling stopped for ' + self._name)

    @abstractmethod
    def _poll_and_record(self):
        """Pollt den Sensor und schreibt die Daten in die Datenbank.
        Diese Methode muss von der Kindklasse implementiert werden. Sie sollte den Sensor pollen und die
        Daten in die Datenbank schreiben. Sie wird im Polling-Thread aufgerufen.
        Die Implementierung sollte in einer Schleife laufen und die isPolling-Flag überprüfen. Wenn sie
        auf False gesetzt wird, sollte die Schleife beendet werden.
        """
        pass

    @staticmethod
    def _print_to_str(*args, **kwargs) -> str:
        """Gibt die Argumente als String zurück.
        Diese Funktion gibt die Argumente als String zurück. Sie wird verwendet, um die Argumente für
        das Logging zu formatieren.
        """
        output = io.StringIO()
        print(*args, file=output, **kwargs)
        contents = output.getvalue()
        output.close()
        return contents

    def _print_log(self, level: int, *args, **kwargs):
        """Loggt eine Nachricht mit dem gegebenen Logging-Level."""
        self._sensor_logger.log(level, self._print_to_str(*args, **kwargs, end=''))

    def _print_dbg(self, *args, **kwargs):
        """Loggt eine Nachricht mit dem Debug-Level."""
        self._print_log(logging.DEBUG, *args, **kwargs)

    def _print_err(self, *args, **kwargs):
        """Loggt eine Nachricht mit dem Error-Level."""
        self._print_log(logging.ERROR, *args, **kwargs)

    def _print_val(self, *args, is_discarded: bool = False, **kwargs):
        """Loggt einen Wert.
        Wenn log_values in der Konfiguration auf False gesetzt ist, wird nichts geloggt.
        Wenn is_discarded auf True gesetzt ist und log_discarded in der Konfiguration auf False
        gesetzt ist, wird nichts geloggt.
        Wenn is_discarded auf True gesetzt ist, wird das Logging-Level DEBUG verwendet, ansonsten VALUE.

        :param args: Die Argumente, die geloggt werden sollen.
        :param is_discarded: Ist der Wert verworfen worden?
        :param kwargs: Weitere Argumente für das Logging.
        """
        if not self._log_values:
            return

        if is_discarded:
            logging_level = logging.DEBUG
            if not self._log_discarded:
                return
        else:
            logging_level = logging.VALUE

        self._print_log(logging_level, *args, **kwargs)

    def _print_info(self, *args, **kwargs):
        """Loggt eine Nachricht mit dem Info-Level."""
        self._print_log(logging.INFO, *args, **kwargs)

    def _require_fields(self, fields: list[str], data: Dict, dict_name: str) -> bool:
        """Stellt sicher, dass ein Dictionary alle erforderlichen Felder enthält.
        Diese Funktion überprüft, ob ein Dictionary alle erforderlichen Felder enthält. Sie gibt True
        zurück, wenn alle Felder vorhanden sind, und False, sobald mindestens ein Feld fehlt.

        :param fields: Die Liste der erforderlichen Felder.
        :param data: Das Dictionary, das überprüft werden soll.
        :param dict_name: Der Name des Dictionarys, der in der Log-Nachricht verwendet wird.

        :return: True, wenn alle Felder vorhanden sind, False, sobald mindestens ein Feld fehlt.
        """

        for field in fields:
            if field not in data:
                self._print_val('Got', dict_name, 'without', field, ':', data, is_discarded=True)
                return False

        return True
