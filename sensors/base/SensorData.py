import time
from abc import abstractmethod
from typing import Dict, List

## \namespace sensors.base.SensorData
# Dieses Modul enthält die Klasse SensorData, die einen Datensatz eines Sensors repräsentiert.


class SensorDataField:
    """Repräsentiert ein Feld in einem Datensatz.
    Ein Feld besteht aus einem Namen, einem Datentyp und einem Standardwert.
    Der Standardwert ist optional und kann weggelassen werden.
    Der Standardwert kann auch eine Funktion sein, die den Standardwert berechnet.

    Attributes:
        name: Der Name des Feldes
        datatype: Der Datentyp des Feldes
        default: Der Standardwert des Feldes
    """

    def __init__(self, name: str, datatype: str, default: any = None):
        """Initialisiert ein neues SensorDataField-Objekt.

        :param name: Der Name des Feldes
        :param datatype: Der Datentyp des Feldes
        :param default: Der Standardwert des Feldes
        """
        self.name = name
        self.datatype = datatype
        self.default = default

    def fill_from_args(self, output: Dict, args: Dict):
        """Füllt das Feld aus den gegebenen Argumenten.
        Wenn das Feld in den Argumenten vorhanden ist, wird es in den Ausgabedaten gespeichert.
        Andernfalls wird der Standardwert verwendet, falls vorhanden, andernfalls wird ein ValueError ausgelöst.
        Wenn als Standardwert eine Funktion angegeben ist, wird diese Funktion aufgerufen und das Ergebnis verwendet.

        :param output: Die Ausgabedaten, in die das Feld geschrieben wird
        :param args: Die Eingabedaten, aus denen das Feld gelesen wird

        :raises ValueError: Wenn das Feld nicht in den Eingabedaten vorhanden ist und kein Standardwert vorhanden ist
        """

        if self.name in args:
            output[self.name] = args[self.name]
        else:
            if self.default is None:
                raise ValueError(f'{self.name} is required')

            # If the default is a function, call it
            if callable(self.default):
                output[self.name] = self.default()
            else:
                output[self.name] = self.default


class SensorData(Dict):
    """Repräsentiert einen Datensatz eines Sensors.
    Dies ist die Basisklasse für alle Datensätze, die von Sensoren aufgenommen werden.
    Sie enthält die Daten, die von einem Sensor aufgenommen werden, und die Methoden, um diese Daten in eine Datenbank
    zu schreiben.
    """

    @staticmethod
    @abstractmethod
    def get_fields() -> List[SensorDataField]:
        """Gibt die Standardfelder für einen Datensatz zurück.
        Die Kindklassen muss diese Methode überschreiben, um die spezifischen Felder für den Datensatz hinzuzufügen.
        Die Funktion der Kindklasse sollten die Standardfelder der Basisklasse hinzufügen und die Liste zurückgeben.
        Jeder Sensor hat die folgenden Felder:
        - timestamp: Der Zeitstempel, zu dem die Daten aufgenommen wurden
        - sensor_name: Der Name des Sensors
        - port: Der Port, an dem der Sensor angeschlossen ist

        Beispiel Kindklasse:

        class MySensorData(SensorData):
            @staticmethod
            def get_fields() -> List[SensorDataField]:
                return DefaultFields.default_fields() + DefaultFields.gyroscope_fields() + [
                    SensorDataField('measured_value', 'REAL'),
                    SensorDataField('my_metadata', 'TEXT', 'N/A')
                ]

            def __init__(self, table_name: str = 'my_sensor_data', **kwargs):
                super().__init__(table_name=table_name, **kwargs)


        :return: Eine Liste von SensorDataField-Objekten, die die Felder für den Datensatz repräsentieren.
        """
        return DefaultFields.default_fields()

    @staticmethod
    def create_sql_table_str(table_name: str, fields: List[SensorDataField]) -> str:
        """Erstellt die SQL-Anweisung, um die Tabelle für diesen Datensatz in der Datenbank zu erstellen.
        Hier wird die List der Felder in eine SQL-Anweisung umgewandelt, die die Tabelle erstellt.
        Die SQL-Anweisung wird als String zurückgegeben.
        Die Form der SQL-Anweisung ist 'CREATE TABLE IF NOT EXISTS table_name (field1 datatype1 NOT NULL, ...)'.

        :param table_name: Der Name der Tabelle
        :param fields: Die Felder der Tabelle
        :return: Die SQL-Anweisung, um die Tabelle für diesen Datensatz in der Datenbank zu erstellen
        """

        field_type_list = ', '.join([f.name + ' ' + f.datatype + ' NOT NULL' for f in fields])
        sql_table_str_template = 'CREATE TABLE IF NOT EXISTS {0} ({1})'
        return sql_table_str_template.format(table_name, field_type_list)

    @staticmethod
    def create_sql_insert_str(table_name: str, fields: List[SensorDataField]) -> str:
        """Erstellt die SQL-Anweisung, um einen Datensatz in die Tabelle zu schreiben.
        Hier wird die List der Felder in eine SQL-Anweisung umgewandelt, die einen Datensatz in die Tabelle schreibt.
        Die SQL-Anweisung wird als String zurückgegeben.
        Die Form der SQL-Anweisung ist 'INSERT INTO table_name (field1, ...) VALUES (:field1, ...)'.
        Zum Ausführen der SQL-Anweisung muss ein Dictionary mit den Feldern als Schlüssel und den Werten als Werten
        bereitgestellt werden. Die Werte werden dann in die SQL-Anweisung eingesetzt.

        :param table_name: Der Name der Tabelle
        :param fields: Die Felder der Tabelle
        :return: Die SQL-Anweisung, um einen Datensatz in die Tabelle zu schreiben
        """

        field_name_list = ', '.join([f.name for f in fields])
        field_insert_name_list = ', '.join([':' + f.name for f in fields])
        sql_insert_str_template = 'INSERT INTO {0} ({1}) VALUES ({2})'
        return sql_insert_str_template.format(table_name, field_name_list, field_insert_name_list)

    def __init__(self, table_name: str = 'sensor_data', **kwargs):
        """Initialisiert ein neues SensorData-Objekt.
        Dieser Konstruktor füllt die Felder des Datensatzes aus den gegebenen Argumenten und den Standardwerten.
        Die Standardfelder werden aus der Methode get_default_fields der Kindklasse abgerufen.

        Examples:

        messergebnisse = []
        # führe 10 Messungen durch
        for _ in range(10);
            # hole die Daten von irgendeiner Messung
            my_data_dict = some_measurement_function()

            # Erstelle ein neues MySensorData-Objekt
            messergebnisse.append(MySensorData(
                table_name='my_table',
                **my_data_dict
            ))

        :param table_name: Der Name der Tabelle
        :param kwargs: Die Felder des Datensatzes

        Raises:
            ValueError: wenn ein Feld in den Argumenten fehlt und kein Standardwert vorhanden ist
        """

        super().__init__()

        all_fields = self.get_fields()
        self.table_name = table_name

        # set the values using the args and the default values
        for field in all_fields:
            field.fill_from_args(output=self, args=kwargs)


class DefaultFields:
    """Eine Klasse mit Methoden, um die Felder für verschiedene Sensortypen zu erhalten.
    Alle Sensoren müssen die standardmäßigen Felder haben, die in der Methode default_fields definiert sind.
    Zusätzlich können Sensoren spezifische Felder haben, die in den Methoden für die spezifischen Sensortypen
    definiert sind. Diese Klasse hat Methoden, um Felder für gängige Sensortypen zu erhalten. Die Methoden sind alle
    so definiert, dass es zu keinem Konflikt zwischen denen kommt. So können Sensoren, die verschiedene Datentypen
    aufnehmen, dieselbe Tabelle verwenden, um ihre Daten zu speichern.
    """

    @staticmethod
    def default_fields():
        """Gibt die Standardfelder für Sensordaten zurück."""
        return [
            SensorDataField('timestamp', 'REAL', time.time),
            SensorDataField('sensor_name', 'TEXT'),
            SensorDataField('port', 'TEXT', 'N/A')
        ]

    @staticmethod
    def accelerometer_fields():
        """Gibt die Standardfelder für Beschleunigungssensordaten zurück."""
        return [
            SensorDataField('accel_x', 'REAL'),
            SensorDataField('accel_y', 'REAL'),
            SensorDataField('accel_z', 'REAL')
        ]

    @staticmethod
    def gyroscope_fields():
        """Gibt die Standardfelder für Gyroskopdaten zurück."""
        return [SensorDataField('gyro_x', 'REAL'), SensorDataField('gyro_y', 'REAL'), SensorDataField('gyro_z', 'REAL')]
