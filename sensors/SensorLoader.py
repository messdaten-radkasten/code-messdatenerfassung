from typing import Dict

from sensors.base.SensorBase import SensorBase

## \namespace sensors
# Dieses Modul enthält alle Klassen und Funktionen, die für die Sensoren benötigt werden.

## \namespace sensors.base
# Dieses Modul enthält die Basisklassen, die für alle Sensoren benötigt werden.
# Hier sind die Klassen, die die Schnittstelle für die Sensoren definieren.
# Die Basisklassen sind abstrakt und sollten nicht direkt instanziiert werden.
# Sie enthalten die gemeinsame Funktionalität, die von mehreren Sensoren benötigt wird.

## \namespace sensors.implementations
# Dieses Modul enthält die Implementierungen der Sensoren.
# Hier sind die Klassen, die die Basisklassen aus \ref sensors.base erweitern.
# Jede Klasse in diesem Modul stellt einen bestimmten Sensor dar.
# Hier sind die GPS und ADXL357 Sensoren implementiert.

## \namespace sensors.SensorLoader
# Dieses Modul enthält eine Funktion, die Sensoren aus einer JSON-Konfiguration laden kann.


class SensorLoader:
    sensor_types: Dict[str, type] = {}

    @staticmethod
    def register_sensor(config_name: str, sensor_type: type):
        if config_name in SensorLoader.sensor_types:
            return

        SensorLoader.sensor_types[config_name] = sensor_type

    @staticmethod
    def load_sensor(json_sensor_config: Dict, database, log_discarded: bool, log_values: bool) -> SensorBase:
        """Lädt einen Sensor aus einer JSON-Konfiguration und gibt ein Sensor-Objekt zurück.
        Diese Funktion ist ein Factory-Methoden-ähnliches Konstrukt, das je nach Typ des Sensors
        den entsprechenden Sensor initialisiert und zurückgibt. Die JSON-Konfiguration muss
        mindestens einen Schlüssel 'type' enthalten, der den Typ des Sensors angibt.
        Siehe \ref docs/config_options.md für die möglichen Typen und deren Konfigurationsoptionen.

        :param json_sensor_config: JSON-Konfiguration des Sensors
        :param database: Die Datenbank, in die die Sensordaten geschrieben werden
        :param log_discarded: Sollen verworfene Sensordaten geloggt werden?
        :param log_values: Sollen Sensordaten geloggt werden?

        :return: Das initialisierte Sensor-Objekt mit der gegebenen Konfiguration
        """

        args = json_sensor_config
        args['db'] = database.connect()
        if 'log_discarded' not in args:
            args['log_discarded'] = log_discarded

        if 'log_values' not in args:
            args['log_values'] = log_values

        if 'type' not in json_sensor_config:
            raise ValueError('Sensor configuration must have a type')

        type_str = json_sensor_config['type']
        if type_str not in SensorLoader.sensor_types:
            raise ValueError(f'Unknown sensor type: {type_str}')

        sensor_type = SensorLoader.sensor_types[type_str]

        return sensor_type(**args)
