import math
from typing import List

## \namespace utils
# Das utils Modul enthält verschiedene Hilfsfunktionen, die in verschiedenen Teilen des Programms verwendet werden.
# Für die Implementierung neuer Sensoren sollte nichts in diesem Modul bearbeitet werden sollen.
# Stattdessen sollte ein neues Modul im sensors Paket erstellt werden.

## \namespace utils.NumberUtils
# Dieses Modul enthält Funktionen, die mit Zahlen arbeiten.
# Die Funktionen in diesem Modul sind hauptsächlich für die Verarbeitung von Zahlen in der
# Kommunikation mit Sensoren und anderen Geräten gedacht.


def combine_bytes(
    data: List[int],
    bit_count: int,
    twos_complement: bool = True,
    low_bit_first: bool = False,
    low_byte_last: bool = True,
    partial_byte_last: bool = False,
    partial_byte_right: bool = True,
    scale_result: bool = False
):
    """Kombiniert Bytes zu einer Zahl.
    Diese Funktion kombiniert eine Liste von Bytes zu einer Zahl. Die Bytes müssen in der Reihenfolge, in der sie
    kombiniert werden sollen, übergeben werden.

    :param data: Die Liste der Bytes.
    :param bit_count: Die Anzahl der Bits, die kombiniert werden sollen.
    :param twos_complement: Gibt an, ob das Ergebnis als Zweierkomplement interpretiert werden soll.
    :param low_bit_first: Gibt an, ob das niederwertigste Bit zuerst kommt.
    :param low_byte_last: Gibt an, ob das niederwertigste Byte das letzte in der Liste ist.
    :param partial_byte_last: Das unvollständige Byte soll am Anfang oder am Ende interpretiert werden.
    :param partial_byte_right: Das unvollständige Byte soll Rechts- oder Linksbündig interpretiert werden.
    :param scale_result: Das Ergebnis soll auf den Wertebereich 0.0 bis 1.0 bzw. -1.0 bis 1.0 skaliert werden.
    """

    num_bytes = math.ceil(bit_count / 8.0)
    partial_bits = bit_count % 8

    if low_byte_last:
        start_index = 0
        end_index = num_bytes
        step = 1
    else:
        start_index = num_bytes - 1
        end_index = -1
        step = -1

    if partial_byte_last:
        partial_byte_index = num_bytes - 1
    else:
        partial_byte_index = 0

    combined_value = 0
    for i in range(start_index, end_index, step):
        value = data[i] & 0xFF
        if low_bit_first:
            value = rotate_int(value, 8)

        if i == partial_byte_index:
            if partial_byte_right:
                value = value & (0xFF >> partial_bits)
            else:
                value = value >> (8 - partial_bits)

            combined_value = (combined_value << partial_bits) | value
        else:
            combined_value = (combined_value << 8) | value

    if twos_complement:
        combined_value = apply_twos_complement(combined_value, bit_count)

    if scale_result:
        if twos_complement:
            return convert_to_float(combined_value, bit_count - 1)
        else:
            return convert_to_float(combined_value, bit_count)
    else:
        return combined_value


def rotate_int(value: int, bit_length: int = 32) -> int:
    """Dreht die Bitreihenfolge eines Integers um.
    Diese Funktion dreht die Bitreihenfolge eines Integers um.

    :param value: Der Integer Wert.
    :param bit_length: Die Anzahl der Bits des Integers.
    """
    result = 0
    for i in range(bit_length):
        result = (result << 1) | (value & 1)
        value = value >> 1
    return result


def apply_twos_complement(value: int, bit_count: int) -> int:
    """Wendet das Zweierkomplement auf einen Integer an.
    Diese Funktion interpretiert den Integer als Zweierkomplement und wendet das Zweierkomplement darauf an.
    Wenn das bit an Stelle bit_count-1 gesetzt ist, wird der Wert als negativ interpretiert.
    Im Falle eines positiven Wertes wird der Wert unverändert zurückgegeben.

    :param value: Der Integer Wert.
    :param bit_count: Die Anzahl der Bits des Integers.
    """

    twos_comp_mask = 0x01 << (bit_count - 1)
    if value & twos_comp_mask == 0:
        return value
    inverse_mask = 0xFFFFFFFF >> (32 - bit_count)
    return -(value ^ inverse_mask) - 1


def convert_to_float(value: int, bit_count: int) -> float:
    """Wandelt einen normalisierten Integer in einen Float um.
    In dieser Function wird angenommen, dass der Integer Wert einen Wertebereich von 0 bis 2^(bit_count)-1 hat.
    Falls der Integer im Zweierkomplement ist, sollte bit_count-1 als Parameter übergeben werden, da die effektive
    Anzahl der Bits um 1 verringert ist.

    :param value: Der Integer Wert.
    :param bit_count: Die Anzahl der Bits des Integers.
    """
    max_value = float((0x01 << bit_count) - 1)
    return float(value) / max_value
