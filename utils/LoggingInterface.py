import datetime
import logging
import os
import sys

## \namespace utils.LoggingInterface
# Diese Datei stellt eine Schnittstelle für das Logging-Interface bereit.


def setup_logging(main_dir: str = os.path.dirname(__file__), log_level: str = 'INFO'):
    """Initialisiert das Logging-Interface.
    Diese Funktion sollte am Anfang des Programms aufgerufen werden, um das Logging-Interface zu initialisieren.
    Das Logging-Interface loggt sowohl in eine Log-Datei als auch in die Konsole.
    Die Log-Datei wird im Ordner logs im gleichen Verzeichnis wie das main Skript gespeichert.
    Der Code ruft auch die Funktion _add_values_log_level auf, um das Log-Level VALUE zu definieren.
    :param main_dir: Das Verzeichnis, in dem das Hauptskript ausgeführt wird.
    :param log_level: Das Log-Level, das in der Konsole verwendet werden soll.
    """

    # Create the value logging level
    _add_values_log_level()

    # Set up logging
    # By default, the log file is saved in the logs folder in the same directory as the script
    # The log file name is log followed by the current date and time
    log_file_name = 'log' + datetime.datetime.now().strftime('%Y-%m-%d:%H:%M:%S') + '.log'
    logs_folder_path = os.path.join(main_dir, 'logs')
    os.makedirs(logs_folder_path, exist_ok=True)
    log_file_path = os.path.join(logs_folder_path, log_file_name)

    # Get the logging level from the command line arguments
    console_level = logging.getLevelName(log_level.upper())

    # create the console logging handler
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setLevel(console_level)

    # create the file logging handler
    file_handler = logging.FileHandler(log_file_path, encoding='utf-8')
    file_handler.setLevel(logging.DEBUG)

    # Set up the logging configuration
    # The logging interface should log to the log file and the console
    # The logging level of the console is set to INFO
    # The logging level of the log file is set to DEBUG
    logging.basicConfig(
        encoding='utf-8',
        level=logging.DEBUG,
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        handlers=[file_handler, console_handler]
    )


def _add_values_log_level():
    """Fügt das Log-Level VALUE hinzu.
    Das Log-Level VALUE wird verwendet, um Werte zu loggen.
    Wenn dieses Log Level bereits definiert ist, wird eine AttributeError ausgelöst.
    """

    if hasattr(logging, 'VALUE'):
        raise AttributeError('VALUE already defined in logging module')
    if hasattr(logging, 'value'):
        raise AttributeError('value already defined in logging module')
    if hasattr(logging.getLoggerClass(), 'value'):
        raise AttributeError('value already defined in logger class')

    def log_for_level(self, message, *args, **kwargs):
        if self.isEnabledFor(15):
            self._log(15, message, args, **kwargs)

    def log_to_root(message, *args, **kwargs):
        logging.log(15, message, *args, **kwargs)

    logging.addLevelName(15, 'VALUE')
    setattr(logging, 'VALUE', 15)
    setattr(logging.getLoggerClass(), 'value', log_for_level)
    setattr(logging, 'value', log_to_root)
