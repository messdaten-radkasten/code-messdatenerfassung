import os
import semver
from typing import Dict
import json

## \namespace utils.config
# Diese Klasse lädt die Konfiguration des Programms aus einer JSON-Datei und speichert sie in einem Dictionary.


class config(Dict):
    """Diese Klasse lädt die Konfiguration des Programms aus einer JSON-Datei und speichert sie in einem Dictionary.
    Wenn die Datei nicht existiert, wird die Standardkonfiguration geladen. Die Standardkonfiguration wird auch
    gespeichert, wenn die Datei nicht existiert. Die Klasse überprüft nicht, ob die Sensor- und Datenbankkonfigurationen
    gültig sind. Es wird davon ausgegangen, dass die Unterkonfigurationen von den jeweiligen Klassen überprüft werden.

    Attributes:
        configPath (str): Der Pfad zur Konfigurationsdatei.
        configVersion (str): Die unterstützte Version der Konfigurationsdatei.
    """
    configVersion = semver.format_version(1, 0, 0)

    def __init__(self, file: str):
        """Initialisiert die Konfiguration, indem die Konfigurationsdatei geladen wird.
        Wenn die Datei nicht existiert, wird die Standardkonfiguration geladen und gespeichert.
        Siehe _load_default_config für mehr Informationen über die Standardkonfiguration.
        """

        super().__init__()

        if file is None:
            raise ValueError('The file parameter cannot be None')

        self.configPath = os.path.abspath(file)

        if os.path.exists(self.configPath):
            self._load()
        else:
            self._load_default_config()
            self._save()

    def _load(self):
        """Lädt die Konfiguration aus der JSON-Datei und speichert sie im Dictionary.

        Raises:
            ValueError: Wenn die Datei nicht existiert.
            ValueError: Wenn die Konfigurationsdatei eine nicht unterstützte Version hat.
            ValueError: Wenn erforderte Schlüssel in der Konfigurationsdatei fehlen.
        """

        json_data = {}
        with open(self.configPath, 'r') as fileP:
            json_data = json.load(fileP)

        if 'version' not in json_data:
            raise ValueError('The config file is missing the version key')
        else:
            self['version'] = json_data['version']

        if semver.compare(json_data['version'], self.configVersion) < 0:
            raise ValueError('The config file is out of date. Please update it.')

        self['log_discarded'] = json_data.get('log_discarded', False)
        self['log_values'] = json_data.get('log_values', True)

        if 'dbConfig' not in json_data:
            raise ValueError('The config file is missing the dbConfig key')
        else:
            self['dbConfig'] = json_data['dbConfig']

        if 'sensorConfig' not in json_data:
            raise ValueError('The config file is missing the sensorConfig key')
        else:
            self['sensorConfig'] = json_data['sensorConfig']

    def _save(self):
        """Speichert die Konfiguration im Dictionary als JSON-Datei."""
        with open(self.configPath, 'w') as fileP:
            json.dump(self, fileP, indent=4)

    def _load_default_config(self):
        """Lädt die Standardkonfiguration in das Dictionary.
        Die Standardkonfiguration ist das folgende JSON Objekt:

        {
            "log_discarded": false,
            "log_values": true,
            "version": "1.0.0",
            "sensorConfig": [
                {
                    "type": "GPS",
                    "timeout": 60,
                    "polling_rate": 10.0
                }
            ],
            "dbConfig": {
                "append_date": true,
                "base_name": "data",
                "directory": "dataBases"
            }
        }

        """

        self['log_discarded'] = False
        self['log_values'] = True
        self['version'] = self.configVersion
        self['sensorConfig'] = []

        # Add the default database configuration
        self['dbConfig'] = {'append_date': True, 'base_name': 'data', 'directory': 'dataBases'}

        # Add a GPS sensor to the default config
        self['sensorConfig'].append({'type': 'GPS', 'timeout': 60, 'polling_rate': 10.0})
