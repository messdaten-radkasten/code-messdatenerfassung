import datetime
import multiprocessing.connection
import os
import sqlite3
import time
from typing import List, Dict

from sensors.base.SensorData import SensorData
import threading
import logging

## \namespace utils.DBConnector
# Klassen zum Interagieren mit einer SQLite-Datenbank.


class SensorTypeCache:
    """Ein Cache für die Sensortypen.
    Diese Klasse cached die SQL Insert statements für die Sensortypen und Tabellennamen Kombinationen.
    Es wird außerdem eine Funktion bereitgestellt, um Sensordaten in einem Befehl zum Cache hinzuzufügen und dann
    in die Datenbank zu schreiben.

    Attributes:
        _cache: Das Dict der gecachten Sensortypen
    """

    class CachedSensorType:
        """Ein gecachter Sensortyp.
        Diese Klasse stellt einen gecachten Sensortyp dar. Sie speichert den Sensortyp, den Tabellennamen und das
        SQL Insert Statement für die Kombination uas beidem. Bei der Initialisierung wird die Tabelle in der Datenbank
        erstellt und das SQL Insert Statement generiert.

        Attributes:
            sensor_type: Der Datentyp des Sensors (Kindklasse von SensorData)
            table_name: Der Tabellenname
            insert_sql: Das SQL Insert Statement
        """

        def __init__(self, sensor_type: type, table_name: str, cursor: sqlite3.Cursor):
            """ Initialisiert den gecachten Sensortyp.
            Hier wird die Tabelle in der Datenbank erstellt und das SQL Insert Statement generiert.

            :param sensor_type: Der Datentyp des Sensors (Kindklasse von SensorData)
            :param table_name: Der Tabellenname
            :param cursor: Der Datenbankcursor
            """

            self.sensor_type = sensor_type
            if not issubclass(sensor_type, SensorData):
                raise ValueError('sensor_type must be a subclass of SensorData')

            self.table_name = table_name

            # call the create_sql_table_str method of the sensor_type
            all_fields = getattr(sensor_type, 'get_fields')()

            table_create_sql = getattr(sensor_type, 'create_sql_table_str')(table_name=table_name, fields=all_fields)
            cursor.execute(table_create_sql)

            self.insert_sql = getattr(sensor_type, 'create_sql_insert_str')(table_name=table_name, fields=all_fields)

    _cache: Dict[str, CachedSensorType] = {}

    @staticmethod
    def _get_cache_id(sensor_type: type, table_name):
        """Gibt die ID für den Cache zurück.
        Diese Funktion gibt den Cache-ID-String für den Sensortyp und den Tabellennamen zurück.
        """
        return f'{sensor_type.__name__}_{table_name}'

    @staticmethod
    def _get_and_add(sensor_type: type, table_name: str, cursor: sqlite3.Cursor) -> CachedSensorType:
        """Holt den gecachten Sensortyp aus dem Cache und fügt ihn hinzu, wenn er nicht vorhanden ist.
        Diese Funktion durchsucht den Cache nach einem gecachten Sensortyp und gibt ihn zurück, wenn er gefunden wird.
        Wenn der gecachte Sensortyp nicht gefunden wird, wird er erstellt, in den Cache eingefügt und zurückgegeben.

        :param sensor_type: Der Datentyp des Sensors (Kindklasse von SensorData)
        :param table_name: Der Tabellenname
        :param cursor: Der Datenbankcursor
        """
        cache_id = SensorTypeCache._get_cache_id(sensor_type, table_name)
        if cache_id in SensorTypeCache._cache:
            return SensorTypeCache._cache[cache_id]

        new_cached = SensorTypeCache.CachedSensorType(sensor_type, table_name, cursor)
        SensorTypeCache._cache[cache_id] = new_cached
        return new_cached

    @staticmethod
    def execute(sensor_data, cursor: sqlite3.Cursor):
        """Fügt die Sensordaten in die Datenbank ein und cached neue Sensortypen.
        Diese Funktion fügt die Sensordaten in die Datenbank ein und cached Sensortypen, die noch nicht im Cache sind.
        Sie verwendet den SensorData-Typ und den Tabellennamen, um den gecachten Sensortyp zu finden oder zu erstellen.

        :param sensor_data: Das Sensordatenobjekt, das in die Datenbank geschrieben werden soll.
        :param cursor: Der Datenbankcursor
        """

        table_name = sensor_data.table_name
        sensor_type = type(sensor_data)
        cached = SensorTypeCache._get_and_add(sensor_type, table_name, cursor)
        cursor.execute(cached.insert_sql, sensor_data)


class DBConnector:
    """Eine Klasse zum Schreiben von Sensordaten in eine SQLite-Datenbank.

    Die DBConnector-Klasse ist für das Schreiben von Sensordaten in eine SQLite-Datenbank verantwortlich.
    Sie verwendet einen Worker-Thread, um die Daten in die Datenbank zu schreiben.
    Alle Sensordaten werden in dem Worker-Thread in die Datenbank geschrieben.
    Diese Klasse bekommt die Daten in Form von SensorData-Objekten und schreibt diese dann Asynchron in die Datenbank.

    Beim Zugriff auf die Warteschlange muss ein Mutex verwendet werden, um die Datenintegrität zu gewährleisten.
    Am besten den Zugriff wie folgt schützen:
    ```
    with self.queue_mutex:
        self.queued_data.append(value)
    ```

    Attributes:
        db_path: Der Pfad der aktuellen Datenbank Datei
        conn: Die Datenbankverbindung
        cursor: Der Datenbankcursor
        db_logger: Das Logger-Objekt für das Datenbank-Logging
        queue_mutex: Ein Mutex um den Zugriff auf die Queue zu schützen.
        queued_data: Die Warteschlange für die Sensordaten. Der Zugriff muss durch den queue_mutex geschützt werden.
        alive: Die Einstellung, ob der Worker-Thread noch aktiv sein soll
        work_thread: Der Worker-Thread auf dem die Daten in die Datenbank geschrieben werden
        worker_sleep: Die Zeit, die der Worker-Thread zwischen den Schreibvorgängen wartet (in Sekunden)
        worker_mutex_timeout: Die Zeit, die der Worker-Thread auf das Erlangen des Mutex wartet (in Sekunden)
        backup_interval_m: Das Intervall in Minuten, in dem die Datenbank gesichert werden soll (0 = keine Sicherung)
        backup_max_files: Die maximale Anzahl von Sicherungsdateien, die aufbewahrt werden sollen (0 = keine Begrenzung)
        backup_subdir: Das Unterverzeichnis, in dem die Sicherungsdateien gespeichert werden sollen
        backup_base_name: Der Basisname der Sicherungsdatei (ohne timestamp und Endung)
        last_backup_time: Der Zeitpunkt der letzten Sicherung
        last_backups: Eine Liste der letzten Sicherungsdateien
    """

    def __init__(self, db_config: dict):
        """ Der Konstruktor des DBConnector-Objekts.
        Er erstellt die Datenbankverbindung und startet den Worker-Thread.

        :param db_config: Die Konfiguration für die Datenbankverbindung
        """
        self.db_logger = logging.getLogger('DBConnector')

        db_name = db_config.get('base_name', 'data')
        if db_config.get('append_date', True):
            current_date = datetime.datetime.now().strftime('%Y-%m-%d')
            db_name += current_date

        db_dir = db_config.get('directory', 'dataBases')
        db_dir = os.path.join(os.path.dirname(__file__), '..', db_dir)
        os.makedirs(db_dir, exist_ok=True)

        self.db_path = os.path.join(db_dir, db_name + '.db')

        self.worker_sleep = float(db_config.get('worker_sleep', 0.1))

        self.backup_interval_m = float(db_config.get('backup_interval_m', 60))
        self.backup_max_files = int(db_config.get('backup_max_files', 10))
        self.backup_subdir = db_config.get('backup_subdir', 'backup')
        self.backup_subdir = os.path.join(db_dir, self.backup_subdir)
        os.makedirs(self.backup_subdir, exist_ok=True)
        self.backup_base_name = db_name
        self.last_backup_time = time.time_ns()

        self.conn: sqlite3.Connection
        self.cursor: sqlite3.Cursor
        self.last_backups = []
        self.alive = True

        self.connections_mutex = threading.Lock()
        self.connections_list: List[multiprocessing.connection.Connection] = []

        self.work_thread = threading.Thread(target=self.__worker)
        self.work_thread.start()

    def __del__(self):
        """Der Destruktor der Klasse.
        Er schließt die Datenbankverbindung und wartet auf das Beenden des Worker-Threads.
        """
        self.close()

    def __worker(self):
        """Der Worker-Thread, der die Sensordaten in die Datenbank schreibt.
        Diese Funktion erstellt die Datenbankverbindung und schreibt die Sensordaten in die Datenbank.
        """
        self.db_logger.info('Opening database connection to ' + os.path.abspath(self.db_path))

        # Create the database connection
        self.conn = sqlite3.connect(self.db_path)
        self.cursor = self.conn.cursor()

        self.db_logger.info('Database connection opened')

        # The worker loop. This loop will run until the alive flag is set to False
        while self.alive:
            local_data = []

            # Get the data from the connections
            with self.connections_mutex:
                for conn in self.connections_list:
                    while conn.poll():
                        local_data.append(conn.recv())

            # Write the data to the database
            for data in local_data:
                # Cache the type and insert the data
                SensorTypeCache.execute(data, self.cursor)

            # Commit the changes
            self.conn.commit()

            # Check if a backup is needed
            # If not needed, sleep for a while
            if not self.__backup_db():
                time.sleep(self.worker_sleep)

    def __backup_db(self) -> bool:
        """Erstellt eine Sicherungskopie der Datenbank.
        Diese Funktion erstellt eine Sicherungskopie der Datenbank.
        Die Sicherungskopie wird in einem Unterverzeichnis des Datenbankverzeichnisses gespeichert.
        Das Unterverzeichnis und die maximale Anzahl von Sicherungsdateien können in der Konfiguration festgelegt werden
        """
        if self.backup_max_files < 0:
            return False

        if self.backup_interval_m <= 0:
            return False

        current_time = time.time_ns()
        if (current_time - self.last_backup_time) / 6e+10 < self.backup_interval_m:
            return False
        self.last_backup_time = current_time

        # create the filename for the backup
        backup_file = os.path.join(
            self.backup_subdir,
            self.backup_base_name + '_' + datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '.db'
        )

        # if the maximum number of backup files is reached, delete the oldest one
        if len(self.last_backups) >= self.backup_max_files:
            os.remove(self.last_backups.pop(0))

        self.last_backups.append(backup_file)

        # actually create the backup of the sqlite database
        with sqlite3.connect(backup_file) as backup_conn:
            self.db_logger.info('Creating backup of the database')
            self.conn.backup(backup_conn)

        return True

    def connect(self) -> multiprocessing.connection.Connection:
        """Erstellt eine Pipe-Verbindung zum DBConnector.
        Diese Funktion erstellt eine Pipe-Verbindung zum DBConnector und gibt die Kindverbindung zurück.
        Die Elternverbindung wird in einer Liste gespeichert, um sie später zu schließen und die Daten entgegenzunehmen.

        :return: Die Kindverbindung zum DBConnector
        """

        parent_conn, child_conn = multiprocessing.connection.Pipe()
        with self.connections_mutex:
            self.connections_list.append(parent_conn)

        return child_conn

    def close(self):
        """Schließt die Datenbankverbindung und wartet auf das Beenden des Worker-Threads.
        Diese Funktion schließt die Datenbankverbindung und wartet auf das Beenden des Worker-Threads.
        Wenn der Worker-Thread bereits beendet ist, wird diese Funktion sofort beendet.
        """
        if not self.alive:
            return

        self.alive = False
        self.work_thread.join()
        self.work_thread = None
