import logging
from typing import List, Dict
import smbus2
import threading
from multiprocessing import connection, Pipe
from abc import ABC

## \namespace utils.I2CConnector
# Diese Datei stellt eine Abstraktionsschicht für den I2C-Bus bereit.


class I2CMessage(ABC):
    """Diese Klasse ist ein Interface für I2C-Nachrichten.
    Die execute-Methode muss in einer Kindklasse implementiert werden.
    Der I2C-Connector wird dann die execute-Methode aufrufen, um die Nachricht zu senden.
    Während der Ausführung der execute-Methode hat die Funktion exklusiven Zugriff auf den I2C-Bus.
    Deswegen sollte die execute-Methode so schnell wie möglich ausgeführt werden und auf keinen Fall eine Kopie
    vom bus speichern.

    Attributes:
        repeating (bool): Soll die Nachricht mehrmals ausgeführt werden?
    """

    def __init__(self, repeating: bool = False):
        """Der konstruktor der Nachricht.
        Args:
            repeating (bool): Soll die Nachricht mehrmals ausgeführt werden?
        """
        self.repeating = repeating

    def execute(self, bus: smbus2.SMBus) -> Dict:
        """Führt die Nachricht aus.
        Args:
            bus (smbus2.SMBus): Der I2C-Bus, auf dem die Nachricht ausgeführt werden soll.
        Returns:
            dict: Ein Dictionary mit den Ergebnissen der Nachricht.
        """
        raise NotImplementedError('I2CMessage.execute must be implemented in a child class')


class I2CConnector:
    """Eine Abstraktionsschicht für den I2C-Bus.
    Diese Klasse stellt eine Schnittstelle für den Zugriff auf den I2C-Bus bereit.
    Sie kümmert sich um die Verwaltung der I2C-Busse und stellt sicher, dass der Zugriff auf den Bus
    thread-sicher ist. Die Kommunikation mit dem I2C-Bus erfolgt über Nachrichten, die von der Klasse
    I2CMessage abgeleitet werden. Die Nachrichten werden in einem eigenen Thread verarbeitet, um die
    Hauptanwendung nicht zu blockieren. Der Austausch von Nachrichten erfolgt über Pipe-Verbindungen.

    Attributes:
        _i2c_logger (logging.Logger): Der Logger für den I2C-Connector.
        _busID: Die ID des I2C-Busses.
        _SmBus (smbus2.SMBus): Die Instanz des I2C-Busses.
        _should_run (bool): Soll der Thread weiterlaufen?
        _connections (List[I2CConnector._connection]): Eine Liste von Verbindungen zu den Clients.
        _work_thread (threading.Thread): Der Thread, der die Nachrichten verarbeitet.

        _bus_list (List): Eine Liste von allen I2CConnectors, die erstellt wurden.
        _bus_list_mutex (threading.Lock): Ein Mutex, um den Zugriff auf die bus_list zu schützen.
    """

    _bus_list: List = []
    _bus_list_mutex = threading.Lock()

    def __init__(self, bus):
        """Der Konstrukteur der Klasse, dieser soll nur in der get_bus-Methode aufgerufen werden.
        Diese Funktion öffnet den I2C-Bus und startet den Worker-Thread.

        :param bus: Die ID des I2C-Busses, welche direkt and den SMBus Konstruktor gegeben wird.
        """
        self._i2c_logger = logging.getLogger('I2CConnector')
        self._busID = bus
        self._SmBus = smbus2.SMBus(bus)
        self._should_run = True
        self._connections: List[I2CConnector._connection] = []
        self._work_thread = threading.Thread(target=self.__do_work)
        self._work_thread.start()

    def close(self):
        """Schließt den I2C-Bus und stoppt den Worker-Thread.
        Falls der Worker-Thread bereits gestoppt wurde, wird diese Funktion sofort beendet.
        """
        if not self._should_run:
            return

        self._should_run = False
        self._work_thread.join()

    def __del__(self):
        """Der Destruktor der Klasse.
        Dieser ruft die close-Methode auf, um den Worker-Thread zu stoppen und den I2C-Bus zu schließen.
        """
        self.close()

    def connect(self):
        """Erstellt eine Verbindung zum I2C-Bus.
        Diese Funktion erstellt eine Pipe und gibt die Verbindung zurück.
        Die Verbindung kann dann genutzt werden, um Nachrichten an den I2C-Bus zu senden und die Ergebnisse zu erhalten.
        Über die Verbindung sollten nur I2CMessage-Objekte gesendet werden.

        Returns:
            Eine Verbindung zum I2C-Bus vom Typ connection.Connection.
        """
        parent_conn, child_conn = Pipe()
        new_conn = I2CConnector._connection(parent_conn)
        self._connections.append(new_conn)
        return child_conn

    def __do_work(self):
        """Die Hauptfunktion des Worker-Threads.
        Diese Funktion wird so lange ausgeführt, bis should_run auf False gesetzt wird.
        Sie ruft die execute-Methode für jede Verbindung auf und kümmert sich um die Verwaltung der Verbindungen.
        Wenn ein nicht abgefangener Fehler während dem execute der connection auftritt, wird die Verbindung geschlossen.
        """
        while self._should_run:
            for conn in self._connections:
                try:
                    conn.execute(self._SmBus)
                except Exception as e:
                    self._i2c_logger.error('Error while executing message:', e)
                    self._connections.remove(conn)

        for conn in self._connections:
            conn.close()

        self._connections.clear()

    @staticmethod
    def get_bus(bus):
        """Gibt den I2C-Connector für einen bestimmten Bus zurück.
        Diese Funktion stellt sicher, dass nur ein I2C-Connector für jeden Bus erstellt wird.

        :param bus: Die ID des I2C-Busses.
        """
        with I2CConnector._bus_list_mutex:
            for b in I2CConnector._bus_list:
                if b.busID == bus:
                    return b

            new_bus = I2CConnector(bus)
            I2CConnector._bus_list.append(new_bus)

            return new_bus

    @staticmethod
    def stop_all():
        """Stoppt alle I2C-Connectors und leert die Liste."""
        with I2CConnector._bus_list_mutex:
            for bus in I2CConnector._bus_list:
                bus.close()
            I2CConnector._bus_list.clear()

    class _connection:
        """Eine Klasse, die eine Verbindung zu einem Client darstellt.
        Diese Klasse kümmert sich um die Verwaltung einer Verbindung zu einem Client.
        Sie speichert die Verbindung und die wiederholenden Nachrichten.
        Sie stellt auch eine Methode zum Empfangen von Nachrichten bereit.

        Attributes:
            _conn (connection.Connection): Die Verbindung zu einem Client.
            _repeating_messages (List[I2CMessage]): Eine Liste von wiederholenden Nachrichten.
        """

        def __init__(self, conn: connection.Connection):
            """Der Konstrukteur der Klasse.
            :param: conn (connection.Connection): Die Verbindung zu einem Client.
            """

            self._conn = conn
            self._repeating_messages: List[I2CMessage] = []

        def _get_messages(self):
            """Empfängt Nachrichten von der Verbindung.
            Die Funktion liest alle Nachrichten aus der Verbindung.
            Wenn neue wiederholende Nachrichten empfangen werden, werden sie zur Liste der wiederholenden Nachrichten
            hinzugefügt.

            Returns:
                Eine Liste mit allen wiederholenden und neuen nicht wiederholenden Nachrichten.
                Wenn die Verbindung geschlossen wurde, wird eine leere Liste zurückgegeben.
            """

            if self._conn is None:
                return []

            # copy all repeating messages to handle them together with new ones
            messages_to_handle = self._repeating_messages.copy()

            # poll for new messages
            while self._conn.poll():
                try:
                    data = self._conn.recv()
                    if not isinstance(data, I2CMessage):
                        self._conn.send(ValueError('Invalid data type:', type(data)))
                        return

                    messages_to_handle.append(data)

                    # if the new message is repeating, add it to the repeating messages list
                    if data.repeating:
                        self._repeating_messages.append(data)

                except EOFError:
                    raise BrokenPipeError('Connection closed')

            return messages_to_handle

        def execute(self, bus: smbus2.SMBus):
            """Holt alle Nachrichten aus der Verbindung und führt sie aus.
            Die Funktion holt alle Nachrichten aus der Verbindung und führt sie aus.
            Sie sendet die Ergebnisse der Nachrichten zurück an den Client.
            Wenn ein Fehler während der Ausführung einer Nachricht auftritt, wird der Fehler an den Client gesendet.

            Raises:
                BrokenPipeError: Wenn die Verbindung geschlossen wurde.
            """

            messages = self._get_messages()
            responses = []

            for message in messages:
                try:
                    result = message.execute(bus)
                    responses.append(result)
                except Exception as e:
                    self._conn.send(e)

            if len(responses) > 0:
                for resp in responses:
                    self._conn.send(resp)

        def close(self):
            """Schließt die Verbindung und löscht alle wiederholenden Nachrichten.
            Wenn die Verbindung bereits geschlossen wurde, passiert nichts.
            """

            if self._conn is not None:
                self._conn.close()
                self._conn = None
                self._repeating_messages.clear()
