#!/bin/bash

# if doxybook is not installed, install it
if ! [ -e "doc_output/doxybook2/bin/doxybook2" ]; then
    mkdir -p doc_output
    mkdir -p doc_output/doxybook2
    wget https://github.com/matusnovak/doxybook2/releases/download/v1.5.0/doxybook2-linux-amd64-v1.5.0.zip -O doc_output/doxybook2.zip
    unzip doc_output/doxybook2.zip -d doc_output/doxybook2
fi

# remove the old html and xml output
doxygen_subdirs=("html" "xml" "man" "latex" "rtf" "docbook")
for dir in "${doxygen_subdirs[@]}"; do
    rm -rf "doc_output/$dir"
done

# run doxygen
doxygen Doxyfile

# build the pdf from latex if the output is enabled
if [ -e "doc_output/latex/Makefile" ]; then
    cd doc_output/latex || exit
    make
    cd ../..
fi

# get the current documentation by loading the git submodule
git submodule update --init --recursive

# delete the old markdown output
markdown_subdirs=("Classes" "Files" "Namespaces" "Pages" "Modules" "Examples" "images")
for dir in "${markdown_subdirs[@]}"; do
    rm -rf "doc_output/wiki/$dir"
done

# run doxybook
doc_output/doxybook2/bin/doxybook2 -i doc_output/xml -o doc_output/wiki --config docs/doxybook_config.json
