import unittest
import utils.NumberUtils as numberUtils

## \namespace test
# Der Namespace für die Unit Tests.

## \namespace test.test_numberUtil
# Die Unit Tests für das NumberUtil Modul.
# Hier werden die Funktionen des NumberUtil Moduls getestet.
# Hier sollten keine Abhängigkeiten zu anderen Modulen bestehen.


class TestNumberUtil(unittest.TestCase):
    """Die unit tests für das NumberUtil Modul.
    Da NumberUtil nur eine Sammlung von Funktionen ist, die keine Abhängigkeiten haben, sind die Tests sehr einfach.
    Allerdings sind diese Funktionen alle low level bit manipulation, daher ist es wichtig, dass sie korrekt
    funktionieren. Hier wird die korrekte Funktionalität getestet. Nach Änderung an der NumberUtil Klasse sollten
    diese Tests ausgeführt werden, um sicherzustellen, dass die Änderungen keine Fehler verursacht haben.
    """

    def test_rotate_int(self):
        """Testet die rotate_int Funktion.
        Diese soll die bitReihenfolge eines Integers umkehren.
        Da die Funktion nur auf einzelne Bytes aufgerufen wird, werden nur kleine Zahlen getestet.
        """

        self.assertEqual(numberUtils.rotate_int(0b01, 2), 0b10)
        self.assertEqual(numberUtils.rotate_int(0b010, 3), 0b010)
        self.assertEqual(numberUtils.rotate_int(0b100, 3), 0b001)

    def test_partial_bytes_4_bit(self):
        """Testet die combine_bytes Funktion mit 4 Bit Daten.
        Die Funktion soll 4 Bit Daten in einen Integer umwandeln.
        Hier wird getestet, ob die Links und Rechtsbündigkeit korrekt funktioniert.
        Es werden hier keine Bytes getestet, die über die Grenze von 4 Bit hinausgehen.
        """

        def partial_combine(**kwargs):
            self.assertEqual(
                numberUtils.combine_bytes(
                    scale_result=False,
                    twos_complement=False,
                    low_bit_first=False,
                    low_byte_last=True,
                    bit_count=4,
                    **kwargs
                ), 0b1111
            )

        value1 = 0b11110000
        value2 = 0b00001111

        # check if the data in the first half works
        partial_combine(data=[value1], partial_byte_last=True, partial_byte_right=False)
        partial_combine(data=[value1], partial_byte_last=False, partial_byte_right=False)

        # check if the data in the second half works
        partial_combine(data=[value2], partial_byte_last=True, partial_byte_right=True)
        partial_combine(data=[value2], partial_byte_last=False, partial_byte_right=True)

    def test_partial_bytes_12_bit(self):
        """Testet die combine_bytes Funktion mit 12 Bit Daten.
        Die Funktion soll 12 Bit Daten in einen Integer umwandeln.
        Hier wird getestet, ob die Links und Rechtsbündigkeit über die Grenze von 8 Bit hinaus korrekt funktioniert.
        Außerdem wird getestet, ob die Position des partiellen Bytes korrekt ist.
        """

        def partial_combine(**kwargs):
            self.assertEqual(
                numberUtils.combine_bytes(
                    scale_result=False,
                    twos_complement=False,
                    low_bit_first=False,
                    low_byte_last=True,
                    bit_count=12,
                    **kwargs
                ), 0xFFF
            )

        value1 = 0xF0
        value2 = 0x0F

        # partial byte first
        partial_combine(data=[value1, 0xFF], partial_byte_last=False, partial_byte_right=False)
        partial_combine(data=[value2, 0xFF], partial_byte_last=False, partial_byte_right=True)

        # partial byte last
        partial_combine(data=[0xFF, value1], partial_byte_last=True, partial_byte_right=False)
        partial_combine(data=[0xFF, value2], partial_byte_last=True, partial_byte_right=True)

    def test_twos_complement(self):
        """Testet, ob das zweier Komplement korrekt angewendet wird.
        Dies wird sowohl direkt in der apply_twos_complement Funktion getestet, als auch in der combine_bytes Funktion.
        Außerdem wird getestet, ob wenn kein zweier Komplement angewendet wird, das Ergebnis korrekt ist.
        """

        def two_cmp_test(data, twos_complement, expected):
            self.assertEqual(
                numberUtils.combine_bytes(data=data, bit_count=16, twos_complement=twos_complement, scale_result=False),
                expected
            )

        def direct_twos_complement_test(data, expected):
            self.assertEqual(numberUtils.apply_twos_complement(data, 16), expected)

        two_cmp_test([0xFF, 0xFF], True, -1)
        two_cmp_test([0x00, 0x00], True, 0)
        two_cmp_test([0x00, 0x01], True, 1)
        two_cmp_test([0x00, 0x7F], True, 127)
        two_cmp_test([0xFF, 0x80], True, -128)
        two_cmp_test([0xFF, 0xFF], False, 65535)
        two_cmp_test([0x00, 0x00], False, 0)
        two_cmp_test([0x00, 0x01], False, 1)
        two_cmp_test([0x00, 0x7F], False, 127)
        two_cmp_test([0xFF, 0x80], False, 65408)

        direct_twos_complement_test(0xFFFF, -1)
        direct_twos_complement_test(0x0000, 0)
        direct_twos_complement_test(0x0001, 1)
        direct_twos_complement_test(0x007F, 127)
        direct_twos_complement_test(0xFF80, -128)

    def test_simple_combine(self):
        """Testet, ob die combine_bytes Funktion korrekt bytes zusammenführt.
        Hier werden die Rückgabewerte als Hexadezimalzahlen getestet.
        Zweier Komplement wird nicht angewendet.
        So kann sehr einfache überprüft werden, ob die Funktion korrekt bytes zusammenführt.
        """

        def simple_combine(data, bit_count, expected):
            self.assertEqual(
                numberUtils.combine_bytes(data=data, bit_count=bit_count, scale_result=False, twos_complement=False),
                expected
            )

        simple_combine([0xFF, 0xFF], 16, 0xFFFF)
        simple_combine([0x00, 0x00], 16, 0x0000)
        simple_combine([0x00, 0x01], 16, 0x0001)
        simple_combine([0x00, 0x7F], 16, 0x007F)
        simple_combine([0xFF, 0x80], 16, 0xFF80)
        simple_combine([0xFF, 0xFF], 12, 0x0FFF)

    def test_scale_result(self):
        """Testet, ob die scale_result Option korrekt funktioniert.
        Hier wird getestet, ob die Rückgabewerte korrekt normiert sind.
        """

        def scale_test(data, bit_count, two_comp, expected):
            self.assertAlmostEqual(
                numberUtils.combine_bytes(data=data, bit_count=bit_count, scale_result=True, twos_complement=two_comp),
                expected,
                places=4
            )

        scale_test([0xFF, 0xFF], 16, False, 1.0)
        scale_test([0x00, 0x00], 16, False, 0.0)
        scale_test([0x00, 0x01], 16, False, 1.0 / 65535)
        scale_test([0x80, 0x00], 16, True, -1.0)
        scale_test([0x00, 0x00], 16, True, 0.0)
        scale_test([0x00, 0x01], 16, True, 1.0 / 32767)
        scale_test([0x7F, 0xFF], 16, True, 1.0)


if __name__ == '__main__':
    unittest.main()
